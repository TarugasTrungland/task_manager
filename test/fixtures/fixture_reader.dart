import 'dart:io';

class FixtureReader {
  static String get(String path) {
    return File('test/fixtures/$path.json').readAsStringSync();
  }
}
