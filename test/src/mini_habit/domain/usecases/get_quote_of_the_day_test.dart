import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/entities/daily_quote_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_quote_of_the_day.dart';

import 'mock_repository.dart';

void main() {
  late MiniHabitRepo repository;
  late GetQuoteOfTheDay userCase;

  setUp(() {
    repository = MockMiniHabitRepo();
    userCase = GetQuoteOfTheDay(repository);
  });

  final tQuote = DailyQuoteEntity.empty();

  test(
      'should call [MiniHabitRepo.getQuoteOfTheDay] '
      'and return [List<MiniHabitEntity>]', () async {
    // arrange
    when(() => repository.getQuoteOfTheDay())
        .thenAnswer((_) async => Right(tQuote));
    // act
    final result = await userCase.call();
    // assert
    expect(
      result,
      Right<dynamic, DailyQuoteEntity>(tQuote),
    );
    verify(() => repository.getQuoteOfTheDay()).called(1);
    verifyNoMoreInteractions(repository);
  });
}
