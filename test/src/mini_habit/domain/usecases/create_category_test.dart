import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/create_category.dart';

import 'mock_repository.dart';

void main() {
  late MiniHabitRepo repository;
  late CreateCategory userCase;

  setUp(() {
    repository = MockMiniHabitRepo();
    userCase = CreateCategory(repository);
  });

  const tCategoryEntity = CategoryEntity.empty();

  test(
      'should call [MiniHabitRepo.createCategory] '
      'and return [void]', () async {
    // arrange
    when(() => repository.createCategory(title: any(named: 'title')))
        .thenAnswer((_) async => const Right(tCategoryEntity));
    // act
    final result = await userCase.call('test category title');
    // assert
    expect(
      result,
      equals(
        const Right<dynamic, CategoryEntity>(tCategoryEntity),
      ),
    );
    verify(() => repository.createCategory(title: 'test category title'))
        .called(1);
    verifyNoMoreInteractions(repository);
  });
}
