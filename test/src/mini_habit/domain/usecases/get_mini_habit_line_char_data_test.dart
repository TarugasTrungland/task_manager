import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habits_line_char_data.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_mini_habit_line_char_data.dart';

import 'mock_repository.dart';

void main() {
  late MiniHabitRepo repository;
  late GetMiniHabitLineCharData userCase;

  setUp(() {
    repository = MockMiniHabitRepo();
    userCase = GetMiniHabitLineCharData(repository);
  });

  const tCharData = MiniHabitsLineChartData.empty();

  test(
      'should call [MiniHabitRepo.getMiniHabitLineCharData] '
      'and return [MiniHabitEntity]', () async {
    // arrange
    when(() => repository.getMiniHabitLineCharData())
        .thenAnswer((_) async => const Right(tCharData));
    // act
    final result = await userCase();
    // assert
    expect(
      result,
      const Right<dynamic, MiniHabitsLineChartData>(tCharData),
    );
    verify(() => repository.getMiniHabitLineCharData()).called(1);
    verifyNoMoreInteractions(repository);
  });
}
