import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_all_categories.dart';

import 'mock_repository.dart';

void main() {
  late MiniHabitRepo repository;
  late GetAllCategories userCase;

  setUp(() {
    repository = MockMiniHabitRepo();
    userCase = GetAllCategories(repository);
  });

  test(
      'should call [MiniHabitRepo.getAllCategories] '
      'and return [List<CategoryEntity>]', () async {
    // arrange
    when(() => repository.getAllCategories())
        .thenAnswer((_) async => const Right([CategoryEntity.empty()]));
    // act
    final result = await userCase();
    // assert
    expect(
      result,
      equals(
        const Right<dynamic, List<CategoryEntity>>(
          [CategoryEntity.empty()],
        ),
      ),
    );
    verify(() => repository.getAllCategories()).called(1);
    verifyNoMoreInteractions(repository);
  });
}
