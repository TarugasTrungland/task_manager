import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_all_active_minihabits.dart';

import 'mock_repository.dart';

void main() {
  late MiniHabitRepo repository;
  late GetAllActiveMiniHabits userCase;

  setUp(() {
    repository = MockMiniHabitRepo();
    userCase = GetAllActiveMiniHabits(repository);
  });

  test(
      'should call [MiniHabitRepo.getAllActiveMiniHabits] '
      'and return [List<MiniHabitEntity>]', () async {
    // arrange
    when(() => repository.getAllActiveMiniHabits())
        .thenAnswer((_) async => const Right([MiniHabitEntity.empty()]));
    // act
    final result = await userCase.call();
    // assert
    expect(
      result,
      equals(
        const Right<dynamic, List<MiniHabitEntity>>(
          [MiniHabitEntity.empty()],
        ),
      ),
    );
    verify(() => repository.getAllActiveMiniHabits()).called(1);
    verifyNoMoreInteractions(repository);
  });
}
