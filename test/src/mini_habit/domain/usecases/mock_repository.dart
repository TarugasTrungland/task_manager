import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';

class MockMiniHabitRepo extends Mock implements MiniHabitRepo {}
