import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/validate_minihabit.dart';

import 'mock_repository.dart';

void main() {
  late MiniHabitRepo repository;
  late ValidateMiniHabit userCase;

  setUp(() {
    repository = MockMiniHabitRepo();
    userCase = ValidateMiniHabit(repository);
  });

  const tParams = ValidateMiniHabitParams.empty();

  test(
      'should call [MiniHabitRepo.validateMiniHabit] '
      'and return [void]', () async {
    // arrange
    when(
      () => repository.validateMiniHabit(
        id: any(named: 'id'),
        resistance: any(named: 'resistance'),
      ),
    ).thenAnswer((_) async => const Right(null));
    // act
    final result = await userCase.call(tParams);
    // assert
    expect(
      result,
      const Right<dynamic, void>(null),
    );
    verify(
      () => repository.validateMiniHabit(
        id: 0,
        resistance: 1,
      ),
    ).called(1);
    verifyNoMoreInteractions(repository);
  });
}
