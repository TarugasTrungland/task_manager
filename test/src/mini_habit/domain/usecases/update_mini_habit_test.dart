import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/update_mini_habit.dart';

import 'mock_repository.dart';

void main() {
  late MiniHabitRepo repository;
  late UpdateMiniHabit userCase;

  setUp(() {
    repository = MockMiniHabitRepo();
    userCase = UpdateMiniHabit(repository);
    registerFallbackValue(const MiniHabitEntity());
  });

  const tParams = MiniHabitEntity.empty();

  test(
      'should call [MiniHabitRepo.updateMiniHabit] '
      'and return [void]', () async {
    // arrange
    when(
      () => repository.updateMiniHabit(
        miniHabitEntity: any(named: 'miniHabitEntity'),
      ),
    ).thenAnswer((_) async => const Right(null));
    // act
    final result = await userCase(tParams);
    // assert
    expect(
      result,
      const Right<dynamic, void>(null),
    );
    verify(
      () => repository.updateMiniHabit(miniHabitEntity: tParams),
    ).called(1);
    verifyNoMoreInteractions(repository);
  });
}
