import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_mini_habit_by_id.dart';

import 'mock_repository.dart';

void main() {
  late MiniHabitRepo repository;
  late GetMiniHabitById userCase;

  setUp(() {
    repository = MockMiniHabitRepo();
    userCase = GetMiniHabitById(repository);
  });

  const tMiniHabit = MiniHabitEntity.empty();

  test(
      'should call [MiniHabitRepo.getMiniHabitById] '
      'and return [MiniHabitEntity]', () async {
    // arrange
    when(() => repository.getMiniHabitById(id: any(named: 'id')))
        .thenAnswer((_) async => const Right(tMiniHabit));
    // act
    final result = await userCase(0);
    // assert
    expect(
      result,
      const Right<dynamic, MiniHabitEntity>(tMiniHabit),
    );
    verify(() => repository.getMiniHabitById(id: 0)).called(1);
    verifyNoMoreInteractions(repository);
  });
}
