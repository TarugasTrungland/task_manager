import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/core/errors/failure.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habits_line_char_data.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_mini_habit_line_char_data.dart';
import 'package:task_manager/src/mini_habit/presentation/stats_screen/bloc/stats_bloc.dart';

class MockGetMiniHabitLineCharData extends Mock
    implements GetMiniHabitLineCharData {}

void main() {
  late GetMiniHabitLineCharData getMiniHabitLineCharData;
  late StatsBloc bloc;

  setUp(() {
    getMiniHabitLineCharData = MockGetMiniHabitLineCharData();
    bloc = StatsBloc(getMiniHabitLineCharData: getMiniHabitLineCharData);
    //registerFallbackValue(tCreateUserParam);
  });
  tearDown(() => bloc.close());

  const tMiniHabitsLineChartData = MiniHabitsLineChartData.empty();
  const tServerFailure =
      ServerFailure(message: 'test failure', statusCode: 500);

  test('initial state should be [StatsInitial]', () {
    // assert
    expect(bloc.state, const StatsInitial());
  });

  group('getMiniHabitLineCharData in statsBloc', () {
    blocTest<StatsBloc, StatsState>(
      'should emit [StatsLoaded] '
      'when StatsLoad added and getMiniHabitLineCharData successful',
      build: () {
        when(() => getMiniHabitLineCharData())
            .thenAnswer((_) async => const Right(tMiniHabitsLineChartData));
        return bloc;
      },
      act: (bloc) => bloc.add(const StatsLoad()),
      expect: () => [
        const StatsLoaded(miniHabitsStat: tMiniHabitsLineChartData),
      ],
      verify: (_) {
        verify(() => getMiniHabitLineCharData()).called(1);
        verifyNoMoreInteractions(getMiniHabitLineCharData);
      },
    );
    blocTest<StatsBloc, StatsState>(
      'should emit[OnMiniHabitError] '
      'when LoadingMiniHabits throws error',
      build: () {
        when(() => getMiniHabitLineCharData())
            .thenAnswer((_) async => const Left(tServerFailure));
        return bloc;
      },
      act: (bloc) => bloc.add(const StatsLoad()),
      expect: () => [
        const StatsLoadingError(
          failure: tServerFailure,
          miniHabitsStat: MiniHabitsLineChartData.empty(),
        ),
      ],
      verify: (_) {
        verify(() => getMiniHabitLineCharData()).called(1);
        verifyNoMoreInteractions(getMiniHabitLineCharData);
      },
    );
  });
}
