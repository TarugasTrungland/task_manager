import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/core/errors/failure.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_all_active_minihabits.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/validate_minihabit.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/bloc/mini_habit_tracker_screen_bloc/mini_habit_bloc.dart';

class MockGetAllActiveMiniHabits extends Mock
    implements GetAllActiveMiniHabits {}

class MockValidateMiniHabit extends Mock implements ValidateMiniHabit {}

void main() {
  late GetAllActiveMiniHabits getAllActiveMiniHabits;
  late ValidateMiniHabit validateMiniHabit;
  late MiniHabitBloc bloc;
  const tMiniHabitEntity = MiniHabitEntity.empty();
  const tValidateParams = ValidateMiniHabitParams.empty();
  const tServerFailure =
      ServerFailure(message: 'test failure', statusCode: 500);
  setUp(() {
    getAllActiveMiniHabits = MockGetAllActiveMiniHabits();
    validateMiniHabit = MockValidateMiniHabit();
    bloc = MiniHabitBloc(
      getAllActiveMiniHabits: getAllActiveMiniHabits,
      validateMiniHabit: validateMiniHabit,
    );
    registerFallbackValue(tValidateParams);
    //registerFallbackValue(tCreateUserParam);
  });

  tearDown(() => bloc.close());
  test('initial state should be [LoadingMiniHabits]', () {
    // assert
    expect(bloc.state, LoadingMiniHabits());
  });
  group('getAllActiveMiniHabits in minihabitBloc', () {
    blocTest<MiniHabitBloc, MiniHabitState>(
      'should emit[ MiniHabitLoaded] '
      'when LoadingMiniHabits added and getAllActiveMiniHabits successful',
      build: () {
        when(() => getAllActiveMiniHabits())
            .thenAnswer((_) async => const Right([tMiniHabitEntity]));
        return bloc;
      },
      act: (bloc) => bloc.add(const LoadMiniHabits()),
      expect: () => [
        const MiniHabitLoaded(miniHabits: [tMiniHabitEntity]),
      ],
      verify: (_) {
        verify(() => getAllActiveMiniHabits()).called(1);
        verifyNoMoreInteractions(getAllActiveMiniHabits);
      },
    );
    blocTest<MiniHabitBloc, MiniHabitState>(
      'should emit[OnMiniHabitError] '
      'when LoadingMiniHabits throws error',
      build: () {
        when(() => getAllActiveMiniHabits())
            .thenAnswer((_) async => const Left(tServerFailure));
        return bloc;
      },
      act: (bloc) => bloc.add(const LoadMiniHabits()),
      expect: () => [
        OnMiniHabitError(
          errorMessage: tServerFailure.errorMessage,
          miniHabits: const [],
        ),
      ],
      verify: (_) {
        verify(() => getAllActiveMiniHabits()).called(1);
        verifyNoMoreInteractions(getAllActiveMiniHabits);
      },
    );
  });

  group('ValidateMiniHabitEvent on bloc', () {
    blocTest<MiniHabitBloc, MiniHabitState>(
      'should emit [LoadingMiniHabits] '
      'when ValidateMiniHabitEvent added and successful',
      build: () {
        when(() => validateMiniHabit(any()))
            .thenAnswer((_) async => const Right(null));
        when(() => getAllActiveMiniHabits())
            .thenAnswer((_) async => const Right([tMiniHabitEntity]));
        return bloc;
      },
      act: (bloc) => bloc.add(
        ValidateMiniHabitEvent(
          id: tValidateParams.id,
          resistance: tValidateParams.resistance,
        ),
      ),
      expect: () => [
        LoadingMiniHabits(),
        const MiniHabitLoaded(miniHabits: [tMiniHabitEntity]),
      ],
      verify: (_) {
        verify(() => validateMiniHabit(any())).called(1);
        verifyNoMoreInteractions(validateMiniHabit);
      },
    );
  });
  blocTest<MiniHabitBloc, MiniHabitState>(
    'should emit[OnMiniHabitError] '
    'when ValidateMiniHabitEvent throws error',
    build: () {
      when(() => validateMiniHabit(any()))
          .thenAnswer((_) async => const Left(tServerFailure));
      return bloc;
    },
    act: (bloc) => bloc.add(
      ValidateMiniHabitEvent(
        id: tValidateParams.id,
        resistance: tValidateParams.resistance,
      ),
    ),
    expect: () => [
      OnMiniHabitError(
        errorMessage: tServerFailure.errorMessage,
        miniHabits: const [],
      ),
    ],
    verify: (_) {
      verify(() => validateMiniHabit(any())).called(1);
      verifyNoMoreInteractions(validateMiniHabit);
    },
  );
}
