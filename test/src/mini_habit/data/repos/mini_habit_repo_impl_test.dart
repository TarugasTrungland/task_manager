import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/core/errors/exception.dart';
import 'package:task_manager/core/errors/failure.dart';
import 'package:task_manager/src/mini_habit/data/datasources/_collections/export_datasources.dart';
import 'package:task_manager/src/mini_habit/data/datasources/mini_habit/mini_habit_data_source.dart';
import 'package:task_manager/src/mini_habit/data/datasources/model/mini_habits_line_chart_data_model.dart';
import 'package:task_manager/src/mini_habit/data/repos/mini_habit_repo_impl.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/daily_quote_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habits_line_char_data.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/validate_minihabit.dart';

class MockMiniHabitDataSource extends Mock implements MiniHabitDataSource {}

void main() {
  late MiniHabitDataSource localeDataSource;
  late MiniHabitRepoImpl repoImpl;

  setUp(() {
    localeDataSource = MockMiniHabitDataSource();
    repoImpl = MiniHabitRepoImpl(localeDataSource);
    registerFallbackValue(const MiniHabitEntity());
  });

  final tDailyQuoteEntity = DailyQuoteEntity.empty();
  final tDailyQuoteCollection = DailyQuoteCollection.empty();
  const tServerException =
      ServerException(message: 'test error', statusCode: 500);
  const tServerFailure = ServerFailure(message: 'test error', statusCode: 500);
  const tValidateParams = ValidateMiniHabitParams.empty();
  final tMiniHabitCollection = MiniHabitCollection.empty();
  const tMiniHabitEntity = MiniHabitEntity.empty();
  final tCategoryCollection = CategoryCollection.empty();
  const tMiniHabitsLineChartData = MiniHabitsLineChartData.empty();
  const tMiniHabitsLineChartDataModel = MiniHabitsLineChartDataModel.empty();

  group('MiniHabitRepoImpl.getAllActiveMiniHabits', () {
    test(
        'should calls the [MiniHabitRepoImpl.getAllActiveMiniHabits] '
        'and complete successfully when call is successful', () async {
      //arrange
      when(
        () => localeDataSource.getAllActiveMiniHabits(),
      ).thenAnswer((_) async => [tMiniHabitCollection]);

      //act
      final result = await repoImpl.getAllActiveMiniHabits();
      // assert
      expect(result, isA<Right<dynamic, List<MiniHabitEntity>>>());
      verify(() => localeDataSource.getAllActiveMiniHabits()).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
    test(
        'should calls the [MiniHabitRepoImpl.getAllActiveMiniHabits] '
        'and return a [ServerException] when call is unsuccessful', () async {
      //arrange
      when(() => localeDataSource.getAllActiveMiniHabits())
          .thenThrow(tServerException);

      //act
      final result = await repoImpl.getAllActiveMiniHabits();
      // assert
      expect(result, equals(const Left<Failure, dynamic>(tServerFailure)));
      verify(() => localeDataSource.getAllActiveMiniHabits()).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
  });
  group('MiniHabitRepoImpl.getQuoteOfTheDay', () {
    test(
        'should calls the [RemoteDataSource.getQuoteOfTheDay] '
        'and return a [DailyQuoteEntity.defaultQuote] when call returns null',
        () async {
      //arrange

      when(() => localeDataSource.getQuoteOfTheDay())
          .thenAnswer((_) async => null);
      //act
      final result = await repoImpl.getQuoteOfTheDay();
      //assert
      expect(
        result,
        Right<dynamic, DailyQuoteEntity>(
          DailyQuoteEntity.defaultQuote(),
        ),
      );
      verify(() => localeDataSource.getQuoteOfTheDay()).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
    test(
        'should calls the [RemoteDataSource.getQuoteOfTheDay] '
        'and return a [DailyQuoteEntity] when call is successful', () async {
      //arrange

      when(() => localeDataSource.getQuoteOfTheDay())
          .thenAnswer((_) async => tDailyQuoteCollection);
      //act
      final result = await repoImpl.getQuoteOfTheDay();
      //assert
      expect(result, Right<dynamic, DailyQuoteEntity>(tDailyQuoteEntity));
      verify(() => localeDataSource.getQuoteOfTheDay()).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
    test(
        'should calls the [RemoteDataSource.getQuoteOfTheDay] '
        'and return a [ServerFailure] when call is unsuccessful', () async {
      //arrange
      when(() => localeDataSource.getQuoteOfTheDay())
          .thenThrow(tServerException);
      //act
      final actual = await repoImpl.getQuoteOfTheDay();
      //assert
      expect(actual, equals(const Left<Failure, dynamic>(tServerFailure)));
      verify(() => localeDataSource.getQuoteOfTheDay()).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
  });
  group('MiniHabitRepoImpl.validateMiniHabit', () {
    test(
        'should calls the [MiniHabitRepoImpl.validateMiniHabit] '
        'and complete successfully when call is successful', () async {
      //arrange
      when(
        () => localeDataSource.validateMiniHabit(
          id: any(named: 'id'),
          resistance: any(named: 'resistance'),
        ),
      ).thenAnswer((_) async => Future.value());

      //act
      final result = await repoImpl.validateMiniHabit(
        id: tValidateParams.id,
        resistance: tValidateParams.resistance,
      );
      // assert
      expect(result, const Right<dynamic, void>(null));
      verify(() => localeDataSource.validateMiniHabit(id: 0, resistance: 1))
          .called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
    test(
        'should calls the [MiniHabitRepoImpl.validateMiniHabit] '
        'and return a [ServerException] when call is unsuccessful', () async {
      //arrange
      when(
        () => localeDataSource.validateMiniHabit(
          id: any(named: 'id'),
          resistance: any(named: 'resistance'),
        ),
      ).thenThrow(tServerException);

      //act
      final result = await repoImpl.validateMiniHabit(
        id: tValidateParams.id,
        resistance: tValidateParams.resistance,
      );
      // assert
      expect(result, equals(const Left<Failure, dynamic>(tServerFailure)));
      verify(() => localeDataSource.validateMiniHabit(id: 0, resistance: 1))
          .called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
  });
  group('MiniHabitRepoImpl.getMiniHabitById', () {
    test(
        'should calls the [MiniHabitRepoImpl.getMiniHabitById] '
        'and return [MiniHabitEntity] when call is successful', () async {
      //arrange
      when(() => localeDataSource.getMiniHabitById(id: any(named: 'id')))
          .thenAnswer((_) async => tMiniHabitCollection);

      //act
      final result = await repoImpl.getMiniHabitById(id: 0);
      // assert
      expect(
        result,
        Right<dynamic, MiniHabitEntity>(tMiniHabitCollection.toEntity()),
      );
      verify(() => localeDataSource.getMiniHabitById(id: 0)).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
    test(
        'should calls the [MiniHabitRepoImpl.getMiniHabitById] '
        'and return a [ServerException] when the return is null', () async {
      //arrange
      when(() => localeDataSource.getMiniHabitById(id: any(named: 'id')))
          .thenAnswer((_) async => null);

      //act
      final result = await repoImpl.getMiniHabitById(id: 0);
      // assert
      expect(
        result,
        equals(
          const Left<Failure, dynamic>(
            ServerFailure(message: 'Mini habit not found', statusCode: 404),
          ),
        ),
      );
      verify(() => localeDataSource.getMiniHabitById(id: 0)).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
    test(
        'should calls the [MiniHabitRepoImpl.getMiniHabitById] '
        'and return a [ServerException] when call is unsuccessful', () async {
      //arrange
      when(() => localeDataSource.getMiniHabitById(id: any(named: 'id')))
          .thenThrow(tServerException);

      //act
      final result = await repoImpl.getMiniHabitById(id: 0);
      // assert
      expect(result, equals(const Left<Failure, dynamic>(tServerFailure)));
      verify(() => localeDataSource.getMiniHabitById(id: 0)).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
  });
  group('MiniHabitRepoImpl.getAllCategories', () {
    test(
        'should calls the [MiniHabitRepoImpl.getAllCategories] '
        'and return [List<CategoryEntity>] when call is successful', () async {
      //arrange
      when(() => localeDataSource.getAllCategories())
          .thenAnswer((_) async => [tCategoryCollection]);

      //act
      final result = await repoImpl.getAllCategories();
      // assert
      expect(result, isA<Right<dynamic, List<CategoryEntity>>>());
      verify(() => localeDataSource.getAllCategories()).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });

    test(
        'should calls the [MiniHabitRepoImpl.getAllCategories] '
        'and return a [ServerException] when call is unsuccessful', () async {
      //arrange
      when(() => localeDataSource.getAllCategories())
          .thenThrow(tServerException);
      //act
      final result = await repoImpl.getAllCategories();
      // assert
      expect(result, equals(const Left<Failure, dynamic>(tServerFailure)));
      verify(() => localeDataSource.getAllCategories()).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
  });
  group('MiniHabitRepoImpl.createNewCategory', () {
    test(
        'should calls the [MiniHabitRepoImpl.createNewCategory] '
        'and return [void] when call is successful', () async {
      //arrange
      when(() => localeDataSource.createCategory(title: any(named: 'title')))
          .thenAnswer((_) async => tCategoryCollection);

      //act
      final result = await repoImpl.createCategory(title: 'test title');
      // assert
      expect(
        result,
        Right<dynamic, CategoryEntity>(tCategoryCollection.toEntity()),
      );
      verify(() => localeDataSource.createCategory(title: 'test title'))
          .called(1);
      verifyNoMoreInteractions(localeDataSource);
    });

    test(
        'should calls the [MiniHabitRepoImpl.getAllCategories] '
        'and return a [ServerException] when call is unsuccessful', () async {
      //arrange
      when(() => localeDataSource.createCategory(title: any(named: 'title')))
          .thenThrow(tServerException);
      //act
      final result = await repoImpl.createCategory(title: 'test title');
      // assert
      expect(result, const Left<Failure, dynamic>(tServerFailure));
      verify(() => localeDataSource.createCategory(title: 'test title'))
          .called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
  });
  group('MiniHabitRepoImpl.updateMiniHabit', () {
    test(
        'should calls the [MiniHabitRepoImpl.updateMiniHabit] '
        'and complete successfully when call is successful', () async {
      //arrange
      when(
        () => localeDataSource.updateMiniHabit(
          miniHabitEntity: any(named: 'miniHabitEntity'),
        ),
      ).thenAnswer((_) async => Future.value());

      //act
      final result =
          await repoImpl.updateMiniHabit(miniHabitEntity: tMiniHabitEntity);
      // assert
      expect(result, const Right<dynamic, void>(null));
      verify(
        () =>
            localeDataSource.updateMiniHabit(miniHabitEntity: tMiniHabitEntity),
      ).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
    test(
        'should calls the [MiniHabitRepoImpl.updateMiniHabit] '
        'and return a [ServerException] when call is unsuccessful', () async {
      //arrange
      when(
        () => localeDataSource.updateMiniHabit(
          miniHabitEntity: any(named: 'miniHabitEntity'),
        ),
      ).thenThrow(tServerException);

      //act
      final result =
          await repoImpl.updateMiniHabit(miniHabitEntity: tMiniHabitEntity);
      // assert
      expect(result, const Left<Failure, dynamic>(tServerFailure));
      verify(
        () =>
            localeDataSource.updateMiniHabit(miniHabitEntity: tMiniHabitEntity),
      ).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
  });
  group('MiniHabitRepoImpl.getMiniHabitLineCharData', () {
    test(
        'should calls the [MiniHabitRepoImpl.getMiniHabitLineCharData] '
        'and complete successfully when call is successful', () async {
      //arrange
      when(() => localeDataSource.getMiniHabitLineCharData())
          .thenAnswer((_) async => tMiniHabitsLineChartDataModel);
      //act
      final result = await repoImpl.getMiniHabitLineCharData();
      // assert
      expect(
        result,
        const Right<dynamic, MiniHabitsLineChartData>(
          tMiniHabitsLineChartData,
        ),
      );
      verify(
        () => localeDataSource.getMiniHabitLineCharData(),
      ).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
    test(
        'should calls the [MiniHabitRepoImpl.getMiniHabitLineCharData] '
        'and return a [ServerException] when call is unsuccessful', () async {
      //arrange
      when(
        () => localeDataSource.getMiniHabitLineCharData(),
      ).thenThrow(tServerException);

      //act
      final result = await repoImpl.getMiniHabitLineCharData();
      // assert
      expect(result, const Left<Failure, dynamic>(tServerFailure));
      verify(
        () => localeDataSource.getMiniHabitLineCharData(),
      ).called(1);
      verifyNoMoreInteractions(localeDataSource);
    });
  });
}
