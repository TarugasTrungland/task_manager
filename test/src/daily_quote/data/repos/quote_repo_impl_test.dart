import 'dart:ui';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/core/errors/exception.dart';
import 'package:task_manager/core/errors/failure.dart';
import 'package:task_manager/src/daily_quote/data/datasources/quote_remote_datasource.dart';
import 'package:task_manager/src/daily_quote/data/models/quote_model.dart';
import 'package:task_manager/src/daily_quote/data/repos/quote_repo_impl.dart';
import 'package:task_manager/src/daily_quote/domain/entities/quote.dart';

class MockQuoteRemoteDataSource extends Mock implements QuoteRemoteDatasource {}

void main() {
  late QuoteRemoteDatasource mockRemoteDataSource;
  late QuoteRepoImpl quoteRepoImpl;
  const tQuote = QuoteModel.empty();
  const tServerException =
      ServerException(message: 'test exception', statusCode: 500);
  setUp(() {
    mockRemoteDataSource = MockQuoteRemoteDataSource();
    quoteRepoImpl = QuoteRepoImpl(mockRemoteDataSource);
    registerFallbackValue(const Locale('en'));
  });

  group('quote repository getDailyQuote', () {
    test('should return [Quote] when successful', () async {
      // arrange
      when(() => mockRemoteDataSource.getDailyQuote(lang: any(named: 'lang')))
          .thenAnswer((_) async => tQuote);
      // act
      final actual =
          await quoteRepoImpl.getDailyQuote(lang: const Locale('es'));
      // assert
      expect(actual, const Right<dynamic, Quote>(tQuote));
      verify(() => mockRemoteDataSource.getDailyQuote(lang: any(named: 'lang')))
          .called(1);
      verifyNoMoreInteractions(mockRemoteDataSource);
    });
  });
  test('should return [ServerException] when not successful', () async {
    // arrange
    when(() => mockRemoteDataSource.getDailyQuote(lang: any(named: 'lang')))
        .thenThrow(tServerException);
    // act
    final actual = await quoteRepoImpl.getDailyQuote(lang: const Locale('es'));
    // assert
    expect(
      actual,
      Left<Failure, dynamic>(
        ServerFailure(
          message: tServerException.message,
          statusCode: tServerException.statusCode,
        ),
      ),
    );
    verify(() => mockRemoteDataSource.getDailyQuote(lang: any(named: 'lang')))
        .called(1);
    verifyNoMoreInteractions(mockRemoteDataSource);
  });
}
