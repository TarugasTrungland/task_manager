import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:task_manager/src/daily_quote/data/models/quote_model.dart';
import 'package:task_manager/src/daily_quote/domain/entities/quote.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  const tQuoteModel = QuoteModel.empty();
  final fixture = FixtureReader.get('quote');
  final tMap = jsonDecode(fixture) as Map<String, dynamic>;

  group('[QuoteModel] tests', () {
    test('should be a subclass of Quote entity', () {
      // assert
      expect(tQuoteModel, isA<Quote>());
    });

    test('should [QuoteModel.fromJson] create an object', () {
      // act
      final result = QuoteModel.fromJson(fixture);
      // assert
      expect(result, tQuoteModel);
    });
    test('should [QuoteModel.fromMap] create an object', () {
      // act
      final result = QuoteModel.fromMap(tMap);
      // assert
      expect(result, tQuoteModel);
    });
    test('should [QuoteModel.toJson] return a JSON map', () {
      // act
      final result = tQuoteModel.toJson();
      // assert
      expect(result, fixture);
    });
    test('should [QuoteModel.toMap] return a JSON map', () {
      // act
      final result = tQuoteModel.toMap();
      // assert
      expect(result, tMap);
    });
    test('should [QuoteModel.copyWith] return another QuoteModel', () {
      // act
      final result = tQuoteModel.copyWith(id: '404', quote: 'new quote');
      // assert
      expect(result, isA<QuoteModel>());
      expect(result.quote, 'new quote');
      expect(result, isNot(tQuoteModel));
    });
  });
}
