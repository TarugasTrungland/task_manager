import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/core/errors/exception.dart';
import 'package:task_manager/src/daily_quote/data/datasources/quote_api_constants.dart';
import 'package:task_manager/src/daily_quote/data/datasources/quote_remote_datasource.dart';
import 'package:task_manager/src/daily_quote/data/models/quote_model.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockClient extends Mock implements http.Client {}

void main() {
  late http.Client httpClient;
  late QuoteRemoteDatasource remoteDatasource;
  const tQuote = QuoteModel.empty();

  setUp(() {
    httpClient = MockClient();
    remoteDatasource = QuoteRemoteDatasourceImpl(httpClient);
    registerFallbackValue(Uri());
  });
  final fixture = FixtureReader.get('quote_from_server');

  group('quote remote datasource . getDailyQuote', () {
    test(
        'should complete and return [Quote] '
        'when response status code is 200 or 201', () async {
      // arrange
      when(() => httpClient.get(any(), headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response(fixture, 200));
      // act
      final actual =
          await remoteDatasource.getDailyQuote(lang: const Locale('en'));
      // assert
      expect(actual, equals(tQuote));
      verify(
        () => httpClient.get(
          Uri.parse('${QuoteApiConstants.kClientUrl}en'),
          headers: QuoteApiConstants.kAdminHeaders(),
        ),
      ).called(1);
      verifyNoMoreInteractions(httpClient);
    });
    test(
        'should throw [ServerException] '
        'when response status code is not 200 or 201', () async {
      // arrange
      when(() => httpClient.get(any(), headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response('server error', 404));
      // act

      final methodCall = remoteDatasource.getDailyQuote;
      // assert
      expect(
        methodCall(lang: const Locale('en')),
        throwsA(
          const ServerException(message: 'server error', statusCode: 404),
        ),
      );
      verify(
        () => httpClient.get(
          Uri.parse('${QuoteApiConstants.kClientUrl}en'),
          headers: QuoteApiConstants.kAdminHeaders(),
        ),
      ).called(1);
      verifyNoMoreInteractions(httpClient);
    });
  });
}
