import 'dart:ui';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:task_manager/src/daily_quote/domain/entities/quote.dart';
import 'package:task_manager/src/daily_quote/domain/repos/quote_repo.dart';
import 'package:task_manager/src/daily_quote/domain/usecases/get_daily_quote.dart';

class MockAuthenticationRepository extends Mock implements QuoteRepo {}

void main() {
  late QuoteRepo quoteRepo;
  late GetDailyQuote usecase;
  const params = Locale('en');
  const tQuote = Quote.empty();

  setUp(() {
    quoteRepo = MockAuthenticationRepository();
    usecase = GetDailyQuote(quoteRepo);
  });

  test('should call the [QuoteRepo.getDailyQuote', () async {
    // arrange
    when(() => quoteRepo.getDailyQuote(lang: params))
        .thenAnswer((_) async => const Right(tQuote));
    // act
    final actual = await usecase.call(params);
    // assert
    expect(actual, const Right<dynamic, Quote>(tQuote));
    verify(() => quoteRepo.getDailyQuote(lang: params)).called(1);
    verifyNoMoreInteractions(quoteRepo);
  });
}
