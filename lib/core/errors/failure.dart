import 'package:equatable/equatable.dart';
import 'package:task_manager/core/errors/exception.dart';

abstract class Failure extends Equatable {
  const Failure({required this.statusCode, required this.message});
  final String message;
  final int statusCode;
  String get errorMessage => '($statusCode) - $message';

  @override
  List<Object> get props => [message, statusCode];
}

class ServerFailure extends Failure {
  const ServerFailure({required super.message, required super.statusCode});

  ServerFailure.fromException(ServerException exception)
      : this(message: exception.message, statusCode: exception.statusCode);
}
