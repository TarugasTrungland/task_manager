import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_manager/core/services/locator.dart';
import 'package:task_manager/src/mini_habit/presentation/edit_mini_habit_screen/bloc/mini_habit_edit_screen_bloc/mini_habit_edit_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/edit_mini_habit_screen/edit_mini_habit_screen.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/mini_habit_form_bloc/mini_habit_form_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/mini_habit_form_screen.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/bloc/mini_habit_tracker_screen_bloc/mini_habit_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/mini_habit_tracker_screen.dart';
import 'package:task_manager/src/mini_habit/presentation/stats_screen/bloc/stats_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/stats_screen/stats_screen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case MiniHabitTrackerScreen.routeName:
      return _pageBuilder(
        (_) => BlocProvider(
          create: (BuildContext context) => locator<MiniHabitBloc>(),
          child: const MiniHabitTrackerScreen(),
        ),
        settings: settings,
      );
    case EditMiniHabitsScreen.routeName:
      return _pageBuilder(
        (_) => BlocProvider(
          create: (BuildContext context) => locator<MiniHabitEditBloc>(),
          child: const EditMiniHabitsScreen(),
        ),
        settings: settings,
      );
    case MiniHabitFormScreen.routeName:
      final id = settings.arguments! as int;
      return _pageBuilder(
        (_) => BlocProvider(
          create: (BuildContext context) => locator<MiniHabitFormBloc>()
            ..add(
              LoadForm(id),
            ),
          child: MiniHabitFormScreen(
            id: id,
          ),
        ),
        settings: settings,
      );
    case StatsScreen.routeName:
      return _pageBuilder(
        (_) => BlocProvider(
          create: (BuildContext context) => locator<StatsBloc>()
            ..add(
              const StatsLoad(),
            ),
          child: const StatsScreen(),
        ),
        settings: settings,
      );
    default:
      return _pageBuilder(
        (_) => BlocProvider(
          create: (BuildContext context) => locator<MiniHabitBloc>(),
          child: const MiniHabitTrackerScreen(),
        ),
        settings: settings,
      );
  }
}

PageRouteBuilder<dynamic> _pageBuilder(
  Widget Function(BuildContext) page, {
  required RouteSettings settings,
}) {
  return PageRouteBuilder(
    settings: settings,
    transitionsBuilder: (_, animation, __, child) => FadeTransition(
      opacity: animation,
      child: child,
    ),
    pageBuilder: (context, _, __) => page(context),
  );
}

/*  return PageRouteBuilder(
      settings: settings,
      transitionsBuilder: (_, animation, secondAnimation, child) =>
          FadeTransition(
            opacity: animation,
            child: SlideTransition(
              position: tween.animate(
                CurvedAnimation(
                  parent: secondAnimation,
                  curve: curve,
                ),
              ),
            ),
          ),
      pageBuilder: (context, _, __) => page(context));

 */
