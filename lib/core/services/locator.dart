import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:task_manager/core/database/local_database.dart';
import 'package:task_manager/src/daily_quote/data/datasources/quote_remote_datasource.dart';
import 'package:task_manager/src/daily_quote/data/repos/quote_repo_impl.dart';
import 'package:task_manager/src/daily_quote/domain/repos/quote_repo.dart';
import 'package:task_manager/src/daily_quote/domain/usecases/get_daily_quote.dart';

import 'package:task_manager/src/mini_habit/data/datasources/mini_habit/mini_habit_data_source.dart';
import 'package:task_manager/src/mini_habit/data/datasources/mini_habit/mini_habit_data_source_impl.dart';
import 'package:task_manager/src/mini_habit/data/repos/mini_habit_repo_impl.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/create_category.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_all_active_minihabits.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_all_categories.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_mini_habit_by_id.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_mini_habit_line_char_data.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_quote_of_the_day.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/update_mini_habit.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/validate_minihabit.dart';
import 'package:task_manager/src/mini_habit/presentation/edit_mini_habit_screen/bloc/mini_habit_edit_screen_bloc/mini_habit_edit_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/category_selector_dialog_bloc/category_selector_dialog_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/mini_habit_form_bloc/mini_habit_form_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/bloc/mini_habit_tracker_screen_bloc/mini_habit_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/bloc/user_bloc/user_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/stats_screen/bloc/stats_bloc.dart';

GetIt locator = GetIt.instance;
void setupLocator() {
  locator
    //* Database
    ..registerLazySingleton<LocalDatabase>(LocalDatabase.new)

    //blocs
    ..registerFactory(
      () => MiniHabitBloc(
        getAllActiveMiniHabits: locator(),
        validateMiniHabit: locator(),
      ),
    )
    ..registerFactory(
      () => UserBloc(
        getQuoteOfTheDay: locator(),
      ),
    )
    ..registerFactory(
      () => MiniHabitEditBloc(getAllActiveMiniHabits: locator()),
    )
    ..registerFactory(
      () => MiniHabitFormBloc(
        getMiniHabitById: locator(),
        updateMiniHabit: locator(),
      ),
    )
    ..registerFactory(
      () => CategorySelectorDialogBloc(
        getAllCategories: locator(),
        createCategory: locator(),
      ),
    )
    ..registerFactory(
      () => StatsBloc(
        getMiniHabitLineCharData: locator(),
      ),
    )

    // use cases daily_quote
    ..registerLazySingleton(() => GetDailyQuote(locator()))
    // use cases minihabits
    ..registerLazySingleton(() => GetQuoteOfTheDay(locator()))
    ..registerLazySingleton(() => GetAllActiveMiniHabits(locator()))
    ..registerLazySingleton(() => ValidateMiniHabit(locator()))
    ..registerLazySingleton(() => GetMiniHabitById(locator()))
    ..registerLazySingleton(() => GetAllCategories(locator()))
    ..registerLazySingleton(() => CreateCategory(locator()))
    ..registerLazySingleton(() => UpdateMiniHabit(locator()))
    ..registerLazySingleton(() => GetMiniHabitLineCharData(locator()))

    // repositories
    ..registerLazySingleton<QuoteRepo>(() => QuoteRepoImpl(locator()))
    ..registerLazySingleton<MiniHabitRepo>(() => MiniHabitRepoImpl(locator()))

    // data sources
    ..registerLazySingleton<QuoteRemoteDatasource>(
      () => QuoteRemoteDatasourceImpl(locator()),
    )
    ..registerLazySingleton<MiniHabitDataSource>(
      () => MiniHabitDataSourceImpl(locator()),
    )

    // external dependencies
    ..registerLazySingleton(http.Client.new);
}
