import 'package:dartz/dartz.dart';
import 'package:task_manager/core/errors/failure.dart';

typedef ResultVoid = Future<Either<Failure, void>>;
typedef ResultFuture<T> = Future<Either<Failure, T>>;
