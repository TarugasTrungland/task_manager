import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:task_manager/core/services/locator.dart';
import 'package:task_manager/inital_data/mock.dart';
import 'package:task_manager/src/daily_quote/domain/entities/quote.dart';
import 'package:task_manager/src/daily_quote/domain/usecases/get_daily_quote.dart';
import 'package:task_manager/src/mini_habit/data/datasources/_collections/export_datasources.dart';

/// A class representing a local database.
///
/// This class provides methods to initialize and access the Isar database.
class LocalDatabase {
  late final Isar _isar;
  bool _isInitialized = false;

  /// Returns the initialized Isar database instance.
  ///
  /// Throws an [IsarError] if the database has not been initialized.
  Isar get db => _isInitialized
      ? _isar
      : throw IsarError('Isar has not been initialized.');

  /// Initializes the Isar database.
  ///
  /// Throws an [IsarError] if the database has already been initialized.
  Future<void> initialize(String platformLocale) async {
    if (_isInitialized) throw IsarError('Isar has already been initialized.');
    _isar = await _openDB();
    _isInitialized = true;
    await _populateIfEmpty();
    await _updateDailyQuote(platformLocale);
  }

  Future<Isar> _openDB() async {
    final dir = await getApplicationDocumentsDirectory();
    if (Isar.instanceNames.isEmpty) {
      return Isar.open(
        [
          MiniHabitCollectionSchema,
          CategoryCollectionSchema,
          ValidationCollectionSchema,
          DailyQuoteCollectionSchema,
        ],
        directory: dir.path,
      );
    }
    return Future.value(Isar.getInstance());
  }

  Future<void> _populateIfEmpty() async {
    try {
      final minihabits = await _isar.miniHabitCollections.where().findAll();
      if (minihabits.isEmpty) {
        debugPrint('⭕ mini habits empty');
        if (kDebugMode) await _importExample();
      }
    } catch (e) {
      debugPrint('❌ db not found');
      debugPrint(e.toString());
    }
  }

  Future<bool> _isDailyQuoteUpdated() async {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final todayQuote = await _isar.dailyQuoteCollections
        .filter()
        .dateEqualTo(today)
        .findFirst();
    return todayQuote != null;
  }

  Future<void> _setDailyQuote(Quote quote) async {
    final dailyQuote = DailyQuoteCollection()
      ..quote = quote.quote
      ..date = DateTime.parse(quote.date)
      ..author = quote.author;
    await _isar.writeTxn(() async {
      await _isar.dailyQuoteCollections.put(dailyQuote);
    });
  }

  Future<DailyQuoteCollection?> getDailyQuote() async {
    final today = _day(0);
    final result = await _isar.dailyQuoteCollections
        .filter()
        .dateEqualTo(today)
        .findFirst();
    return result;
  }

  DateTime _day(int fromToday) {
    final now = DateTime.now();
    return DateTime(now.year, now.month, now.day + fromToday);
  }

  Future<void> _updateDailyQuote(String locale) async {
    if (!await _isDailyQuoteUpdated()) {
      debugPrint('🫥🫥🫥 daily quote not updated');
      final getDailyQuote = locator<GetDailyQuote>();
      final result = await getDailyQuote.call(Locale(locale));
      await result.fold((failure) {
        if (kDebugMode) {
          print('🔶Daily Quote Server not found!!!🔶');
        }
      }, (quote) async {
        debugPrint('💚New external daily quote 💚');
        await _setDailyQuote(quote);
      });
    }
  }

  Future<void> _importExample() async {
    await _isar.writeTxn(() async {
      await _isar.miniHabitCollections
          .importJson(Mock.miniHabitsJson['miniHabits']!);
    });
    await _isar.writeTxn(() async {
      await _isar.categoryCollections
          .importJson(Mock.categoriesJson['categories']!);
    });

    final miniHabit = await _isar.miniHabitCollections.where().findAll();
    miniHabit.map((e) async {
      if (e.id == 4) {
        e.category.value =
            await _isar.categoryCollections.filter().idEqualTo(3).findFirst();
      } else {
        e.category.value = await _isar.categoryCollections
            .filter()
            .idEqualTo(e.id)
            .findFirst();
      }
      await _isar.writeTxn(() async {
        await e.category.save();
      });
    }).toList();
  }
}
