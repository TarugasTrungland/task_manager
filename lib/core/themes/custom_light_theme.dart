import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:task_manager/core/themes/custom_global_theme.dart';

class CustomLightTheme {
  final Color _primary = const Color.fromRGBO(236, 65, 31, 1);
  final Color _background = const Color.fromRGBO(255, 255, 255, 1);
  ThemeData data({required String deviceType}) {
    final lightTheme = ThemeData(
      useMaterial3: true,
      fontFamily: 'Poppins',
      colorScheme: ColorScheme(
        brightness: Brightness.light,
        primary: _primary,
        onPrimary: Colors.white, //should be  legible on primary
        secondary: Colors.green,
        onSecondary: Colors.white, //should be  legible on secondary
        tertiary:
            const Color.fromRGBO(203, 203, 203, 1), // grey diminished color
        error: Colors.red,
        onError: Colors.white, //should be legible on background
        surface: const Color.fromRGBO(255, 255, 255, 1), // for cards
        onSurface: Colors.black, //should be legible on surface
        // other colors: inverse, tertiary, surfaceVariant, outline,
        // scrim (crystal), primary container
      ),
    );
    return lightTheme.copyWith(
      textTheme: _lightTextTheme(lightTheme.textTheme, deviceType),
      primaryIconTheme: lightTheme.primaryIconTheme.copyWith(
        color: const Color.fromRGBO(182, 182, 182, 1),
      ),
      inputDecorationTheme: _lightInputDecorationTheme(
        lightTheme.inputDecorationTheme,
        deviceType,
      ),
      cardTheme: _lightCardThemeData(lightTheme.cardTheme),
      elevatedButtonTheme: _lightElevatedThemeData(deviceType),
    );
  }

  ElevatedButtonThemeData _lightElevatedThemeData(String deviceType) {
    return CustomGlobalTheme.customElevatedButtonThemeData(
      deviceType,
      color: _primary,
      bgColor: _background,
    );
  }

  CardTheme _lightCardThemeData(CardTheme base) {
    return base.copyWith(
      surfaceTintColor: Colors.transparent,
      color: Colors.white,
      elevation: 2,
    );
  }

  TextTheme _lightTextTheme(TextTheme base, String deviceType) {
    return GoogleFonts.karlaTextTheme(
      CustomGlobalTheme.textThemeData(base, deviceType)
          .merge(Typography().black),
    );
  }

  InputDecorationTheme _lightInputDecorationTheme(
    InputDecorationTheme base,
    String deviceType,
  ) {
    return CustomGlobalTheme.inputDecorationData(
      base,
      deviceType,
      Colors.black,
    );
  }
}
