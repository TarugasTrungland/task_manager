import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:task_manager/core/themes/custom_global_theme.dart';

class CustomDarkTheme {
  final Color _primary = const Color.fromRGBO(236, 65, 31, 1);
  final Color _background = const Color.fromRGBO(40, 40, 40, 1);
  ThemeData data({required String deviceType}) {
    final d = ThemeData(brightness: Brightness.dark);

    final darkTheme = d.copyWith(
      colorScheme: ColorScheme(
        brightness: Brightness.dark,
        primary: _primary,
        onPrimary: Colors.white, //should be  legible on primary
        secondary: const Color.fromRGBO(38, 117, 25, 1),
        onSecondary: Colors.white, //should be  legible on secondary
        tertiary: const Color.fromRGBO(203, 203, 203, 1),
        error: Colors.red,
        onError: Colors.white, //should be legible on background
        surface: _background, // for cards
        onSurface: Colors.white, //should be legible on surface
        // other colors: inverse, tertiary, surfaceVariant, outline,
        // scrim (crystal), primary container
      ),
    );
    return darkTheme.copyWith(
      textTheme: _darkCustomTextTheme(darkTheme.textTheme, deviceType),
      primaryIconTheme: darkTheme.primaryIconTheme.copyWith(
        color: const Color.fromRGBO(182, 182, 182, 1),
      ),
      inputDecorationTheme:
          _darkInputDecorationTheme(darkTheme.inputDecorationTheme, deviceType),
      cardTheme: _darkCustomCardThemeData(darkTheme.cardTheme),
      elevatedButtonTheme: _darkElevatedThemeData(deviceType),
    );
  }

  ElevatedButtonThemeData _darkElevatedThemeData(String deviceType) {
    return CustomGlobalTheme.customElevatedButtonThemeData(
      deviceType,
      color: _primary,
      bgColor: _background,
    );
  }

  CardTheme _darkCustomCardThemeData(CardTheme base) {
    return base.copyWith(
      surfaceTintColor: Colors.transparent,
      color: const Color.fromRGBO(162, 42, 42, 1),
      elevation: 2,
    );
  }

  TextTheme _darkCustomTextTheme(TextTheme base, String deviceType) {
    return GoogleFonts.karlaTextTheme(
      CustomGlobalTheme.textThemeData(base, deviceType)
          .merge(Typography().white),
    );
  }

  InputDecorationTheme _darkInputDecorationTheme(
    InputDecorationTheme base,
    String deviceType,
  ) {
    return CustomGlobalTheme.inputDecorationData(
      base,
      deviceType,
      Colors.white38,
    );
  }
}
