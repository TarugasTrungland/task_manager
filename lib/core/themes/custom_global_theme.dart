import 'package:flutter/material.dart';

class CustomGlobalTheme {
  static TextTheme textThemeData(TextTheme base, String deviceType) {
    var sizeFactor = 1.0;
    if (deviceType == 'tablet') sizeFactor = 1.8;
    return base.copyWith(
      /*
      displayLarge: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: (57 * sizeFactor).roundToDouble(),
      ),
      */
      /*
      displayMedium: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: (46 * sizeFactor).roundToDouble(),
      ),
      */

      displaySmall: TextStyle(
        fontWeight: FontWeight.w700,
        letterSpacing: 0.8,
        fontSize: (36 * sizeFactor).roundToDouble(),
      ),

      /*
      headlineLarge: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: (32 * sizeFactor).roundToDouble(),
      ),
      */
      /*
      headlineMedium: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: (28 * sizeFactor).roundToDouble(),
      ),
      */
      /*
      headlineSmall: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: (24 * sizeFactor).roundToDouble(),
      ),
      */

      titleLarge: TextStyle(
        fontWeight: FontWeight.w800,
        letterSpacing: 0.6,
        fontSize: (19 * sizeFactor).roundToDouble(),
      ),

      /*
      titleMedium: TextStyle(
        fontWeight: FontWeight.w500,
        letterSpacing: 0.15,
        fontSize: (16 * sizeFactor).roundToDouble(),
      ),
      */
      /*
      titleSmall: TextStyle(
        fontWeight: FontWeight.w500,
        letterSpacing: 0.1,
        fontSize: (14 * sizeFactor).roundToDouble(),
      ),
      */

      labelLarge: TextStyle(
        fontWeight: FontWeight.w500,
        letterSpacing: 0.1,
        fontSize: (14 * sizeFactor).roundToDouble(),
      ),
      labelMedium: TextStyle(
        fontWeight: FontWeight.w500,
        letterSpacing: 0.6,
        fontSize: (12 * sizeFactor).roundToDouble(),
        height: 1.8,
      ),
      /* labelSmall: TextStyle(
          fontWeight: FontWeight.w500,
          letterSpacing: 0.6,
          fontSize: (11 * sizeFactor).roundToDouble(),
          height: 1.8),*/

      bodyLarge: TextStyle(
        fontWeight: FontWeight.w400,
        letterSpacing: 0.15,
        fontSize: (16 * sizeFactor).roundToDouble(),
      ),

      /*
      bodyMedium: TextStyle(
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25,
        fontSize: (14 * sizeFactor).roundToDouble(),
      ),
      */
      /*
      bodySmall: TextStyle(
        fontWeight: FontWeight.w400,
        letterSpacing: 0.4,
        fontSize: (12 * sizeFactor).roundToDouble(),
      ),
      */
    );
  }

  static ElevatedButtonThemeData customElevatedButtonThemeData(
      String deviceType,
      {required Color color,
      required Color bgColor,}) {
    var sizeFactor = 1.0;
    if (deviceType == 'tablet') sizeFactor = 1.8;
    final textSize = (16 * sizeFactor).roundToDouble();
    return ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            side: BorderSide(color: color),
            borderRadius: BorderRadius.circular(4),
          ),
          backgroundColor: bgColor,
          foregroundColor: color,
          textStyle: TextStyle(
            fontWeight: FontWeight.w400,
            letterSpacing: 1,
            fontSize: textSize,
          ),
          elevation: 0,),
    );
  }

  static InputDecorationTheme inputDecorationData(
      InputDecorationTheme base, String deviceType, Color color,) {
    return base.copyWith(
        filled: true,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0),
          borderSide: BorderSide(color: color),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0),
          borderSide: BorderSide(width: 2, color: color),
        ),
        contentPadding: const EdgeInsets.all(12),);
  }
}
