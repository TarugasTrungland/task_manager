import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:task_manager/core/database/local_database.dart';
import 'package:task_manager/core/services/locator.dart';
import 'package:task_manager/core/services/router.dart';
import 'package:task_manager/core/themes/custom_light_theme.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/bloc/user_bloc/user_bloc.dart';

import 'core/themes/custom_dark_theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  final platformLocale = Platform.localeName;
  await locator<LocalDatabase>().initialize(platformLocale);
  runApp(MyApp(platformLocale));
}

class MyApp extends StatelessWidget {
  const MyApp(this.locale, {super.key});
  final String locale;
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => locator<UserBloc>()
            ..add(
              Init(Locale(locale)),
            ),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Task Manager',
        theme: CustomLightTheme().data(deviceType: getDeviceType(context)),
        darkTheme: CustomDarkTheme().data(deviceType: getDeviceType(context)),
        localizationsDelegates: const [
          //translation: delegate for MaterialAPP widgets
          GlobalMaterialLocalizations.delegate,
          //translation: delegate for Cupertino widgets
          GlobalCupertinoLocalizations.delegate,
          //translation: delegate RTL
          GlobalWidgetsLocalizations.delegate,
          //translation: delegate current App
          S.delegate,
        ],
        supportedLocales: const [
          Locale('en', ''), // English, no country code
          Locale('es', ''), // Spanish, no country code
        ],
        onGenerateRoute: generateRoute,
      ),
    );
  }
}

String getDeviceType(BuildContext context) {
  final data = MediaQuery.of(context);
  return data.size.shortestSide < 600 ? 'phone' : 'tablet';
}
