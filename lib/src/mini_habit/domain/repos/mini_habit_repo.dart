import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/daily_quote_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habits_line_char_data.dart';

abstract class MiniHabitRepo {
  const MiniHabitRepo();
  ResultFuture<List<MiniHabitEntity>> getAllActiveMiniHabits();
  ResultFuture<DailyQuoteEntity> getQuoteOfTheDay();

  ResultFuture<void> validateMiniHabit({
    required int id,
    required int resistance,
  });
  ResultFuture<MiniHabitEntity> getMiniHabitById({required int id});
  ResultFuture<List<CategoryEntity>> getAllCategories();
  ResultFuture<CategoryEntity> createCategory({required String title});
  ResultFuture<void> updateMiniHabit({
    required MiniHabitEntity miniHabitEntity,
  });
  ResultFuture<MiniHabitsLineChartData> getMiniHabitLineCharData();
}
