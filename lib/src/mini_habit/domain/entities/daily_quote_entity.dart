import 'package:equatable/equatable.dart';

class DailyQuoteEntity extends Equatable {
  const DailyQuoteEntity({this.id, this.quote, this.author, this.date});
  DailyQuoteEntity.empty()
      : this(
          id: 0,
          quote: '_empty.quote',
          author: '_empty.quote',
          date: DateTime(1990),
        );
  DailyQuoteEntity.defaultQuote()
      : this(
          id: 0,
          quote: 'It is not a matter if you fail, but how well you fail',
          author: 'Oscar Auliq Ice',
          date: DateTime(1990),
        );

  final int? id;
  final String? quote;
  final String? author;
  final DateTime? date;

  @override
  List<Object?> get props => [id, quote, author, date];
}
