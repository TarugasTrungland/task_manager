import 'package:equatable/equatable.dart';
import 'package:fl_chart/fl_chart.dart';

class MiniHabitsLineChartData extends Equatable {
  const MiniHabitsLineChartData({
    required this.dayNumbers,
    required this.flSpots,
    required this.resistanceFlSpots,
  });

  const MiniHabitsLineChartData.empty()
      : this(
          dayNumbers: const [0],
          flSpots: const [
            FlSpot(1, 1),
          ],
          resistanceFlSpots: const [
            FlSpot(2, 2),
          ],
        );

  final List<int> dayNumbers;
  final List<FlSpot> flSpots;
  final List<FlSpot> resistanceFlSpots;

  @override
  List<Object?> get props => [dayNumbers, flSpots, resistanceFlSpots];
}
