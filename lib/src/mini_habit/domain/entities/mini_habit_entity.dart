import 'package:equatable/equatable.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';

class MiniHabitEntity extends Equatable {
  const MiniHabitEntity({
    this.id,
    this.title,
    this.description,
    this.category,
  });

  const MiniHabitEntity.empty()
      : this(
          id: 0,
          title: '_empty.title',
          description: '_empty.description',
          category: const CategoryEntity.empty(),
        );
  final CategoryEntity? category;

  final int? id;
  final String? title;
  final String? description;

  @override
  List<Object?> get props => [
        id,
        title,
        description,
        category,
      ];

  MiniHabitEntity copyWith({
    CategoryEntity? category,
    int? id,
    String? title,
    String? description,
  }) {
    return MiniHabitEntity(
      category: category ?? this.category,
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
    );
  }
}
