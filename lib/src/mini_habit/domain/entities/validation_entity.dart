import 'package:equatable/equatable.dart';

class ValidationEntity extends Equatable {
  const ValidationEntity({this.id, this.dateTime, this.resistance});
  ValidationEntity.empty()
      : this(
          id: 0,
          resistance: 0,
          dateTime: DateTime(1990),
        );
  final int? id;
  final DateTime? dateTime;
  final int? resistance;

  @override
  List<Object?> get props => [id, dateTime, resistance];
}
