import 'package:equatable/equatable.dart';

class CategoryEntity extends Equatable {
  const CategoryEntity({this.id, this.title});
  const CategoryEntity.empty()
      : this(
          id: 0,
          title: '_empty.title',
        );
  final int? id;
  final String? title;

  @override
  List<Object?> get props => [id, title];
}
