import 'package:task_manager/core/usecase/usecase.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';

class GetAllActiveMiniHabits
    extends UsecaseWithoutParams<List<MiniHabitEntity>> {
  const GetAllActiveMiniHabits(this._repository);
  final MiniHabitRepo _repository;

  @override
  ResultFuture<List<MiniHabitEntity>> call() {
    return _repository.getAllActiveMiniHabits();
  }
}
