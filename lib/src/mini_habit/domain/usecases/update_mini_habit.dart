import 'package:task_manager/core/usecase/usecase.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';

class UpdateMiniHabit extends UsecaseWithParams<void, MiniHabitEntity> {
  const UpdateMiniHabit(this._repository);
  final MiniHabitRepo _repository;

  @override
  ResultFuture<void> call(MiniHabitEntity params) {
    return _repository.updateMiniHabit(
      miniHabitEntity: params,
    );
  }
}
