import 'package:equatable/equatable.dart';
import 'package:task_manager/core/usecase/usecase.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';

class ValidateMiniHabit
    extends UsecaseWithParams<void, ValidateMiniHabitParams> {
  ValidateMiniHabit(this._repository);
  final MiniHabitRepo _repository;

  @override
  ResultFuture<void> call(ValidateMiniHabitParams params) async => _repository
      .validateMiniHabit(id: params.id, resistance: params.resistance);
}

class ValidateMiniHabitParams extends Equatable {
  const ValidateMiniHabitParams({required this.id, required this.resistance});

  const ValidateMiniHabitParams.empty() : this(id: 0, resistance: 1);
  final int id;
  final int resistance;

  @override
  List<Object?> get props => [id, resistance];
}
