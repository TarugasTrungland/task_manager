import 'package:task_manager/core/usecase/usecase.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';

class CreateCategory extends UsecaseWithParams<void, String> {
  CreateCategory(this._repository);
  final MiniHabitRepo _repository;

  @override
  ResultFuture<CategoryEntity> call(String params) async =>
      _repository.createCategory(title: params);
}
