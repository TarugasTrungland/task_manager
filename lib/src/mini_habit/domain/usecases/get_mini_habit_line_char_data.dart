import 'package:task_manager/core/usecase/usecase.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habits_line_char_data.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';

class GetMiniHabitLineCharData
    extends UsecaseWithoutParams<MiniHabitsLineChartData> {
  const GetMiniHabitLineCharData(this._repository);
  final MiniHabitRepo _repository;

  @override
  ResultFuture<MiniHabitsLineChartData> call() =>
      _repository.getMiniHabitLineCharData();
}
