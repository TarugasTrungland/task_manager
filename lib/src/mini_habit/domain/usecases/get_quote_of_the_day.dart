import 'package:task_manager/core/usecase/usecase.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/mini_habit/domain/entities/daily_quote_entity.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';

class GetQuoteOfTheDay extends UsecaseWithoutParams<DailyQuoteEntity> {
  GetQuoteOfTheDay(this._repository);
  final MiniHabitRepo _repository;

  @override
  ResultFuture<DailyQuoteEntity> call() async => _repository.getQuoteOfTheDay();
}
