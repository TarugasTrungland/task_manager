import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/mini_habit_form_bloc/mini_habit_form_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/category_selection_dialog/category_selector_dialog.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/mini_habit_tracker_screen.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/circular_waiting_widget.dart';

class FormWidget extends StatefulWidget {
  const FormWidget({required this.id, super.key});
  final int? id;

  @override
  State<FormWidget> createState() => _FormWidgetState();
}

class _FormWidgetState extends State<FormWidget> {
  static final GlobalKey<FormState> _formMiniHabitKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final s = S.of(context);

    return BlocListener<MiniHabitFormBloc, MiniHabitFormState>(
      listener: (context, state) {
        if (state.runtimeType == FormSuccess) {
          Navigator.pushNamed(context, MiniHabitTrackerScreen.routeName);
        }
      },
      child: BlocBuilder<MiniHabitFormBloc, MiniHabitFormState>(
        builder: (context, state) {
          switch (state) {
            case FormLoading():
              return Padding(
                padding:
                    EdgeInsets.only(top: BlankSize.getSize(context, times: 4)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: BlankSize.getSize(context, times: 2),
                      width: BlankSize.getSize(context, times: 2),
                      child: const CircularWaitingWidget(),
                    ),
                  ],
                ),
              );
            case FormLoaded():
              return SingleChildScrollView(
                child: Form(
                  key: _formMiniHabitKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: BlankSize.getSize(context, times: 3),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            s.edit,
                            style: Theme.of(context)
                                .textTheme
                                .displaySmall
                                ?.copyWith(
                                  color:
                                      Theme.of(context).colorScheme.onSurface,
                                  shadows: HardShadow.getShadow(context),
                                ),
                          ),
                          ElevatedButton(
                            onPressed: () => context
                                .read<MiniHabitFormBloc>()
                                .add(FormSubmitEvent()),
                            child: Row(
                              children: [
                                const Icon(Icons.save_outlined),
                                const SizedBox(width: 3),
                                Text(s.save_button),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const BlankSpace(onlyVertical: true, times: 2),
                      Text(
                        s.category,
                        style: Theme.of(context)
                            .textTheme
                            .titleLarge
                            ?.copyWith(shadows: HardShadow.getShadow(context)),
                      ),
                      const BlankSpace(onlyVertical: true),
                      GestureDetector(
                        onTap: () async {
                          final result = await showDialog<CategoryEntity>(
                            barrierColor: const Color(0x01000000),
                            context: context,
                            builder: (BuildContext dialogContext) {
                              return CategorySelectorDialog(
                                currentCategory: state.miniHabit.category,
                              );
                            },
                          );

                          if (result != null && context.mounted) {
                            context
                                .read<MiniHabitFormBloc>()
                                .add(FormCategoryUpdateEvent(category: result));
                          }
                        },
                        child: Container(
                          height: BlankSize.getSize(context, times: 4),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Theme.of(context).colorScheme.surface,
                            boxShadow: HardShadow.getBoxShadow(context),
                            border: Border.all(
                              color: Theme.of(context).colorScheme.onSurface,
                            ),
                          ),
                          child: Row(
                            children: [
                              const BlankSpace(),
                              Text(
                                state.miniHabit.category?.title ?? '',
                                style: Theme.of(context).textTheme.labelLarge,
                              ),
                              const Spacer(),
                              const Icon(Icons.arrow_drop_down),
                              const BlankSpace(),
                            ],
                          ),
                        ),
                      ),
                      const BlankSpace(onlyVertical: true, times: 3),
                      Text(
                        s.description,
                        style: Theme.of(context)
                            .textTheme
                            .titleLarge
                            ?.copyWith(shadows: HardShadow.getShadow(context)),
                      ),
                      const BlankSpace(onlyVertical: true),
                      Container(
                        height: BlankSize.getSize(context, times: 4),
                        decoration: BoxDecoration(
                          boxShadow: HardShadow.getBoxShadow(context),
                        ),
                        child: TextFormField(
                          style: Theme.of(context).textTheme.labelLarge,
                          initialValue: state.miniHabit.title,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(
                              top: 10,
                              left: 10,
                              bottom: 10,
                            ), // Adjust to fit within the container
                          ),
                          onChanged: (value) {
                            context.read<MiniHabitFormBloc>().add(
                                  UpdateMiniHabitTitleEvent(
                                    miniHabitTitle: value,
                                  ),
                                );
                            state.miniHabit.copyWith(title: value);
                          },
                        ),
                      ),
                      const BlankSpace(onlyVertical: true),
                    ],
                  ),
                ),
              );

            case FormSuccess():
              return const SizedBox();
            case FormFailure():
              // TODO(dani): Handle this case.
              return const Text('Form error!');
          }
        },
      ),
    );
  }
}
