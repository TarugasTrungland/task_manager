import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/mini_habit_form_bloc/mini_habit_form_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/form_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/vertical_menu_widget.dart';

class MiniHabitFormScreen extends StatelessWidget {
  const MiniHabitFormScreen({required this.id, super.key});
  final int? id;
  static const String routeName = 'form_screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          BlocBuilder<MiniHabitFormBloc, MiniHabitFormState>(
            builder: (context, state) {
              return IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: SafeArea(
                        child: Padding(
                          padding: EdgeInsets.all(BlankSize.getSize(context)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              IconButton(
                                onPressed: () => Navigator.pop(context),
                                icon: const Icon(Icons.arrow_back),
                              ),
                              /*  ElevatedButton(
                                  onPressed: () => print('deleting MH'),
                                  child: Row(
                                    children: [
                                      const Icon(Icons.delete_outline),
                                      Text(s.delete_button)
                                    ],
                                  ))*/
                            ],
                          ),
                        ),
                      ),
                    ),
                    const Spacer(),
                    const VerticalMenuWidget(
                      miniHabitScreen: MiniHabitScreen.edit,
                    ),
                  ],
                ),
              );
            },
          ),
          Container(
            height: 1,
            width: double.infinity,
            color: Theme.of(context).colorScheme.onSurface,
          ),
          MiniHabitBackground(
            fileName: 'edit',
            child: FormWidget(id: id),
          ),
        ],
      ),
    );
  }
}
