part of 'category_selector_dialog_bloc.dart';

sealed class CategorySelectorDialogEvent {
  const CategorySelectorDialogEvent();
}

class LoadCategories extends CategorySelectorDialogEvent {
  LoadCategories(this.currentCategory);
  CategoryEntity? currentCategory;
}

class SelectCategory extends CategorySelectorDialogEvent {
  SelectCategory(this.currentCategory);
  CategoryEntity? currentCategory;
}

class CreateNewCategory extends CategorySelectorDialogEvent {
  CreateNewCategory({required this.isCreatingCategory});
  bool isCreatingCategory;
}

class SaveNewCategory extends CategorySelectorDialogEvent {
  SaveNewCategory({
    required this.newCategoryTitle,
    required this.isCreatingCategory,
  });
  bool isCreatingCategory;
  String newCategoryTitle;
}
