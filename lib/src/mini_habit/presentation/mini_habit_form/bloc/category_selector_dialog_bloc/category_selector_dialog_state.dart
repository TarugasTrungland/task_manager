part of 'category_selector_dialog_bloc.dart';

sealed class CategorySelectorDialogState extends Equatable {
  const CategorySelectorDialogState({
    required this.currentCategory,
    required this.categories,
    required this.isCreatingCategory,
  });
  final CategoryEntity? currentCategory;
  final List<CategoryEntity> categories;
  final bool isCreatingCategory;
}

final class CategorySelectorDialogInitial extends CategorySelectorDialogState {
  CategorySelectorDialogInitial()
      : super(categories: [], currentCategory: null, isCreatingCategory: false);

  @override
  List<Object> get props => [];
}

final class CategorySelectorDialogLoaded extends CategorySelectorDialogState {
  const CategorySelectorDialogLoaded({
    required super.categories,
    required super.currentCategory,
    required super.isCreatingCategory,
  });

  @override
  List<Object> get props =>
      [categories.length, currentCategory!, isCreatingCategory];
}
