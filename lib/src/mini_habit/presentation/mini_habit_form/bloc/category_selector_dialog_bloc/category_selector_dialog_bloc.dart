import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/create_category.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_all_categories.dart';

part 'category_selector_dialog_event.dart';
part 'category_selector_dialog_state.dart';

class CategorySelectorDialogBloc
    extends Bloc<CategorySelectorDialogEvent, CategorySelectorDialogState> {
  CategorySelectorDialogBloc({
    required GetAllCategories getAllCategories,
    required CreateCategory createCategory,
  })  : _getAllCategories = getAllCategories,
        _createCategory = createCategory,
        super(CategorySelectorDialogInitial()) {
    on<LoadCategories>(_onLoadCategories);
    on<SelectCategory>(_onSelectingCategories);
    on<CreateNewCategory>(_onCreatingCategory);
    on<SaveNewCategory>(_onSavingCategory);
  }
  final GetAllCategories _getAllCategories;
  final CreateCategory _createCategory;
  Future<void> _onLoadCategories(
    LoadCategories event,
    Emitter<CategorySelectorDialogState> emit,
  ) async {
    final categories = await _getAllCategories();
    categories.fold(
      (failure) => print('failure'),
      (success) => emit(
        CategorySelectorDialogLoaded(
          categories: success,
          currentCategory: event.currentCategory,
          isCreatingCategory: false,
        ),
      ),
    );
  }

  Future<void> _onSelectingCategories(
    SelectCategory event,
    Emitter<CategorySelectorDialogState> emit,
  ) async {
    final currentState = state as CategorySelectorDialogLoaded;
    emit(
      CategorySelectorDialogLoaded(
        categories: currentState.categories,
        currentCategory: event.currentCategory,
        isCreatingCategory: false,
      ),
    );
  }

  void _onCreatingCategory(
    CreateNewCategory event,
    Emitter<CategorySelectorDialogState> emit,
  ) {
    final currentState = state as CategorySelectorDialogLoaded;
    emit(
      CategorySelectorDialogLoaded(
        categories: currentState.categories,
        currentCategory: currentState.currentCategory,
        isCreatingCategory: event.isCreatingCategory,
      ),
    );
  }

  Future<void> _onSavingCategory(
    SaveNewCategory event,
    Emitter<CategorySelectorDialogState> emit,
  ) async {
    final result = await _createCategory(event.newCategoryTitle);
    result.fold(
      (_) => print('no category created'),
      (success) => add(
        LoadCategories(success),
      ),
    );
  }
}
