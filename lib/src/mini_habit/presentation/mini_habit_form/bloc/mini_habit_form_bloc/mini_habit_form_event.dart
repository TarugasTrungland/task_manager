part of 'mini_habit_form_bloc.dart';

sealed class MiniHabitFormEvent {
  const MiniHabitFormEvent();
}

class LoadForm extends MiniHabitFormEvent {
  const LoadForm(this.id);
  final int? id;
}

class FormSubmitEvent extends MiniHabitFormEvent {}

class FormUpdateEvent extends MiniHabitFormEvent {
  const FormUpdateEvent(this.formData);
  final String formData;
}

class FormCategoryUpdateEvent extends MiniHabitFormEvent {
  const FormCategoryUpdateEvent({required this.category});
  final CategoryEntity category;
}

class UpdateMiniHabitTitleEvent extends MiniHabitFormEvent {
  const UpdateMiniHabitTitleEvent({required this.miniHabitTitle});
  final String miniHabitTitle;
}
