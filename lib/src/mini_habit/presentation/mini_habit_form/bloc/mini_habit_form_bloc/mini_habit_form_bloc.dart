import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_mini_habit_by_id.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/update_mini_habit.dart';

part 'mini_habit_form_event.dart';
part 'mini_habit_form_state.dart';

class MiniHabitFormBloc extends Bloc<MiniHabitFormEvent, MiniHabitFormState> {
  MiniHabitFormBloc({
    required GetMiniHabitById getMiniHabitById,
    required UpdateMiniHabit updateMiniHabit,
  })  : _getMiniHabitById = getMiniHabitById,
        _updateMiniHabit = updateMiniHabit,
        super(const FormLoading()) {
    on<LoadForm>(_onLoadingMiniHabit);
    on<FormSubmitEvent>(_onSubmit);
    on<FormCategoryUpdateEvent>(_onCategoryUpdate);
    on<UpdateMiniHabitTitleEvent>(_onUpdateMiniHabitTile);
  }
  final GetMiniHabitById _getMiniHabitById;
  final UpdateMiniHabit _updateMiniHabit;

  Future<void> _onLoadingMiniHabit(
    LoadForm event,
    Emitter<MiniHabitFormState> emit,
  ) async {
    if (event.id != null) {
      final result = await _getMiniHabitById(event.id!);
      result.fold(
        (failure) => emit(FormFailure(failure.errorMessage)),
        (success) => emit(FormLoaded(success)),
      );
    }
  }

  Future<void> _onSubmit(
    FormSubmitEvent event,
    Emitter<MiniHabitFormState> emit,
  ) async {
    final result = await _updateMiniHabit(
      state.miniHabit,
    );

    result.fold(
      (failure) => emit(
        const FormFailure('notEditedError'),
      ),
      (success) => emit(
        FormSuccess(state.miniHabit),
      ),
    );
  }

  Future<void> _onCategoryUpdate(
    FormCategoryUpdateEvent event,
    Emitter<MiniHabitFormState> emit,
  ) async {
    final currentState = state as FormLoaded;
    emit(FormLoaded(currentState.miniHabit.copyWith(category: event.category)));
  }

  FutureOr<void> _onUpdateMiniHabitTile(
    UpdateMiniHabitTitleEvent event,
    Emitter<MiniHabitFormState> emit,
  ) {
    final currentState = state as FormLoaded;
    final updatedMiniHabit =
        currentState.miniHabit.copyWith(title: event.miniHabitTitle);
    emit(FormLoaded(updatedMiniHabit));
  }
}
