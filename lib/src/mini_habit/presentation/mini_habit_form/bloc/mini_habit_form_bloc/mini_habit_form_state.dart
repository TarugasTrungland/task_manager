part of 'mini_habit_form_bloc.dart';

sealed class MiniHabitFormState extends Equatable {
  const MiniHabitFormState(this.miniHabit);
  final MiniHabitEntity miniHabit;
  @override
  List<Object> get props => [
        miniHabit,

        // miniHabit.category,
        miniHabit.title!,
        // miniHabit.category.value!,
      ];
}

class FormLoading extends MiniHabitFormState {
  const FormLoading() : super(const MiniHabitEntity.empty());
}

class FormLoaded extends MiniHabitFormState {
  const FormLoaded(super.miniHabit);
}

class FormSuccess extends MiniHabitFormState {
  const FormSuccess(super.miniHabit);
}

class FormFailure extends MiniHabitFormState {
  const FormFailure(this.error) : super(const MiniHabitEntity.empty());
  final String error;
}
