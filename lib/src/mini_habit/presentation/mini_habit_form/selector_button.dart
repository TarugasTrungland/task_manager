import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/category_selector_dialog_bloc/category_selector_dialog_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class SelectorButton extends StatelessWidget {
  const SelectorButton({
    required this.isSelected,
    required this.category,
    super.key,
  });

  final bool isSelected;
  final CategoryEntity category;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: BlankSize.getSize(context, times: 0.5),
      ),
      child: GestureDetector(
        onTap: () {
          context
              .read<CategorySelectorDialogBloc>()
              .add(SelectCategory(category));
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(120)),
            color: isSelected
                ? Theme.of(context).colorScheme.primary
                : Theme.of(context).colorScheme.surface,
            border: Border.all(color: Theme.of(context).colorScheme.primary),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: BlankSize.getSize(context, times: 0.5),
              horizontal: BlankSize.getSize(context),
            ),
            child: Text(
              category.title!,
              style: Theme.of(context).textTheme.labelMedium?.copyWith(
                    color: isSelected
                        ? Theme.of(context).colorScheme.surface
                        : Theme.of(context).colorScheme.primary,
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
