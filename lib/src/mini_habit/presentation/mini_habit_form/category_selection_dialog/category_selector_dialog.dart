import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/core/services/locator.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/category_selector_dialog_bloc/category_selector_dialog_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/category_selection_dialog/create_category_form.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/category_selection_dialog/select_category_view.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class CategorySelectorDialog extends StatelessWidget {
  const CategorySelectorDialog({
    super.key,
    this.currentCategory,
  });

  final CategoryEntity? currentCategory;

  @override
  Widget build(BuildContext context) {
    final s = S.of(context);

    return Dialog(
      insetPadding: EdgeInsets.all(BlankSize.getSize(context)),
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Theme.of(context).colorScheme.onSurface),
        borderRadius: BorderRadius.circular(0),
      ),
      elevation: 0,
      child: Padding(
        padding: EdgeInsets.all(
          BlankSize.getSize(context),
        ),
        child: BlocProvider(
          create: (context) => locator<CategorySelectorDialogBloc>()
            ..add(LoadCategories(currentCategory)),
          child: BlocBuilder<CategorySelectorDialogBloc,
              CategorySelectorDialogState>(
            builder: (context, state) {
              switch (state) {
                case CategorySelectorDialogInitial():
                  return SizedBox(
                    width: BlankSize.getSize(context, times: 4),
                    height: BlankSize.getSize(context, times: 4),
                    child: const Center(
                      child: CircularProgressIndicator(strokeWidth: 1),
                    ),
                  );
                case CategorySelectorDialogLoaded():
                  return Padding(
                    padding: EdgeInsets.all(BlankSize.getSize(context)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              s.category,
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                            Visibility(
                              maintainSize: true,
                              maintainAnimation: true,
                              maintainState: true,
                              visible: !state.isCreatingCategory,
                              child: IconButton(
                                onPressed: () => context
                                    .read<CategorySelectorDialogBloc>()
                                    .add(
                                      CreateNewCategory(
                                        isCreatingCategory: true,
                                      ),
                                    ),
                                icon: const Icon(Icons.add_circle_outline),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: BlankSize.getSize(context),
                            bottom: BlankSize.getSize(context, times: 2),
                          ),
                          child: Container(
                            height: 1,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Theme.of(context).colorScheme.onSurface,
                            ),
                          ),
                        ),
                        if (state.isCreatingCategory)
                          CreateCategoryForm(state)
                        else
                          SelectCategoryView(state),
                      ],
                    ),
                  );
              }
            },
          ),
        ),
      ),
    );
  }
}
