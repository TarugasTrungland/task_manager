import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/category_selector_dialog_bloc/category_selector_dialog_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/category_selection_dialog/category_selector.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class SelectCategoryView extends StatelessWidget {
  const SelectCategoryView(this.state, {super.key});
  final CategorySelectorDialogState state;
  @override
  Widget build(BuildContext context) {
    final s = S.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          s.select_one,
          style: Theme.of(context).textTheme.labelLarge,
        ),
        if (state.categories.isNotEmpty)
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            scrollDirection: Axis.horizontal,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CategoriesSelector(
                  currentCategory: state.currentCategory,
                  categories: state.categories,
                  module: 0,
                ),
                CategoriesSelector(
                  currentCategory: state.currentCategory,
                  categories: state.categories,
                  module: 1,
                ),
                CategoriesSelector(
                  currentCategory: state.currentCategory,
                  categories: state.categories,
                  module: 2,
                ),
              ],
            ),
          )
        else
          const Text('no categories'),
        const BlankSpace(times: 2),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ElevatedButton(
              onPressed: () => Navigator.pop(
                context,
                context
                    .read<CategorySelectorDialogBloc>()
                    .state
                    .currentCategory,
              ),
              child: Text(s.save_button),
            ),
          ],
        ),
      ],
    );
  }
}
