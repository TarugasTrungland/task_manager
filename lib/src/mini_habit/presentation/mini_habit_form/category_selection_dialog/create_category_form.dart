import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/bloc/category_selector_dialog_bloc/category_selector_dialog_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class CreateCategoryForm extends StatelessWidget {
  const CreateCategoryForm(this.state, {super.key});
  final CategorySelectorDialogState state;
  static final _createCategoryFormKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    var categoryTitle = '';
    final s = S.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          s.new_category,
          style: Theme.of(context).textTheme.labelLarge,
        ),
        const BlankSpace(),
        Form(
          key: _createCategoryFormKey,
          child: Column(
            children: [
              TextFormField(
                style: Theme.of(context).textTheme.labelLarge,
                decoration: const InputDecoration(
                  contentPadding: EdgeInsets.only(
                    top: 10,
                    left: 10,
                    bottom: 10,
                  ), // Adjust to fit within the container
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                onChanged: (value) {
                  categoryTitle = value;
                },
              ),
              const BlankSpace(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: () => context
                        .read<CategorySelectorDialogBloc>()
                        .add(CreateNewCategory(isCreatingCategory: false)),
                    child: Text(s.cancel_button),
                  ),
                  const BlankSpace(times: 2),
                  ElevatedButton(
                    onPressed: () {
                      if (_createCategoryFormKey.currentState!.validate()) {
                        context.read<CategorySelectorDialogBloc>().add(
                              SaveNewCategory(
                                newCategoryTitle: categoryTitle,
                                isCreatingCategory: false,
                              ),
                            );
                      }
                    },
                    child: Text(s.save_button),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
