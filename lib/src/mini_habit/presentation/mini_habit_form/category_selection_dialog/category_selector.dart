import 'package:flutter/material.dart';

import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/selector_button.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class CategoriesSelector extends StatelessWidget {
  const CategoriesSelector({
    required this.categories,
    required this.currentCategory,
    required this.module,
    super.key,
  });
  final List<CategoryEntity> categories;
  final int module;
  final CategoryEntity? currentCategory;

  @override
  Widget build(BuildContext context) {
    final subList = <CategoryEntity>[];
    categories.asMap().keys.map((k) {
      if (k % 3 == module) subList.add(categories[k]);
    }).toList();
    return Row(
      children: subList.map((e) {
        if (e.title == null || e.title!.isEmpty) {
          return const SizedBox();
        } else {
          return Padding(
            padding: EdgeInsets.only(right: BlankSize.getSize(context)),
            child: SelectorButton(
              isSelected:
                  currentCategory != null && e.id == currentCategory?.id,
              category: e,
            ),
          );
        }
      }).toList(),
    );
  }
}
