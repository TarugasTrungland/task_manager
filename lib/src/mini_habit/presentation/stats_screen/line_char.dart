import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class LineChar extends StatelessWidget {
  const LineChar({
    required this.dayNumbers,
    required this.flSpots,
    required this.resistanceFlSpots,
    super.key,
  });

  final List<int> dayNumbers;
  final List<FlSpot> flSpots;
  final List<FlSpot> resistanceFlSpots;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: BlankSize.getSize(context, times: 12),
      color: Theme.of(context).colorScheme.surface,
      child: LineChart(
        LineChartData(
          borderData: FlBorderData(
            show: true,
            border: Border(
              top: BorderSide(
                color: Theme.of(context).colorScheme.onSurface.withOpacity(0.1),
              ),
              bottom: BorderSide(
                color: Theme.of(context).colorScheme.onSurface.withOpacity(0.5),
              ),
            ),
          ),
          gridData: const FlGridData(
            drawVerticalLine: false,
          ),
          backgroundColor: Theme.of(context).colorScheme.surface,
          lineBarsData: [
            LineChartBarData(
              color: Theme.of(context).colorScheme.primary,
              belowBarData: BarAreaData(
                show: true,
                color: Theme.of(context).colorScheme.primary.withOpacity(0.2),
              ),
              dotData: const FlDotData(
                show: false,
              ),
              spots: flSpots,
            ),
            LineChartBarData(
              isStrokeCapRound: true,
              barWidth: 4,
              curveSmoothness: 0.22,
              isCurved: true,
              gradient: LinearGradient(
                colors: [
                  Theme.of(context).colorScheme.onSurface.withOpacity(0.5),
                  Theme.of(context).colorScheme.onSurface.withOpacity(1),
                ],
              ),
              belowBarData: BarAreaData(
                color: Theme.of(context).colorScheme.primary.withOpacity(0.4),
              ),
              dotData: const FlDotData(
                show: false,
              ),
              spots: resistanceFlSpots,
            ),
          ],
          titlesData: FlTitlesData(
            bottomTitles: AxisTitles(sideTitles: bottomTitles(dayNumbers)),
            rightTitles: const AxisTitles(),
            leftTitles: const AxisTitles(),
            topTitles: const AxisTitles(
              sideTitles: SideTitles(reservedSize: 12),
            ),
          ),
        ),
      ),
    );
  }

  SideTitles bottomTitles(List<int> dayNumbers) => SideTitles(
        showTitles: true,
        getTitlesWidget: (value, meta) {
          final text = dayNumbers[value.toInt()].toString();
          return Padding(
            padding: const EdgeInsets.only(top: 6),
            child: Text(text),
          );
        },
      );
}
