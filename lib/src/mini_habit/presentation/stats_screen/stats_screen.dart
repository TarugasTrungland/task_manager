import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/src/mini_habit/presentation/stats_screen/bloc/stats_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/stats_screen/line_char.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/circular_waiting_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/error_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/vertical_menu_widget.dart';

class StatsScreen extends StatelessWidget {
  const StatsScreen({super.key});
  static const String routeName = 'Stats Screen';

  @override
  Widget build(BuildContext context) {
    final s = S.of(context);
    return Scaffold(
      body: Column(
        children: [
          const Row(
            children: [
              Spacer(),
              VerticalMenuWidget(miniHabitScreen: MiniHabitScreen.stats),
            ],
          ),
          MiniHabitBackground(
            fileName: 'stats',
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: BlankSize.getSize(context, times: 3),
                ),
                Stack(
                  children: [
                    Text(
                      s.stats,
                      style: Theme.of(context).textTheme.displaySmall?.copyWith(
                            color: Theme.of(context).colorScheme.onSurface,
                            shadows: HardShadow.getShadow(context),
                          ),
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: BlankSize.getSize(context, times: 2.5),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12)),
                            color: Theme.of(context).colorScheme.surface,
                            border: Border.all(
                              color: Theme.of(context).colorScheme.onSurface,
                            ),
                          ),
                          child: Center(
                            child: BlocBuilder<StatsBloc, StatsState>(
                              builder: (context, state) {
                                switch (state.runtimeType) {
                                  case StatsInitial:
                                    return Center(
                                      child: Padding(
                                        padding: EdgeInsets.all(
                                          BlankSize.getSize(
                                            context,
                                            times: 2,
                                          ),
                                        ),
                                        child: const SizedBox(
                                          height: 40,
                                          width: 40,
                                          child: CircularWaitingWidget(),
                                        ),
                                      ),
                                    );
                                  case StatsLoaded:
                                    return Padding(
                                      padding: EdgeInsets.all(
                                        BlankSize.getSize(
                                          context,
                                        ),
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.all(
                                          BlankSize.getSize(
                                            context,
                                          ),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              s.this_week,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleLarge
                                                  ?.copyWith(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .onSurface,
                                                    shadows:
                                                        HardShadow.getShadow(
                                                      context,
                                                    ),
                                                  ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                vertical: BlankSize.getSize(
                                                  context,
                                                ),
                                              ),
                                              child: LineChar(
                                                dayNumbers: state
                                                    .miniHabitsStat.dayNumbers,
                                                flSpots: state
                                                    .miniHabitsStat.flSpots,
                                                resistanceFlSpots: state
                                                    .miniHabitsStat
                                                    .resistanceFlSpots,
                                              ),
                                            ),
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                Baseline(
                                                  baseline: BlankSize.getSize(
                                                    context,
                                                    times: 0.5,
                                                  ),
                                                  baselineType:
                                                      TextBaseline.alphabetic,
                                                  child: Text(
                                                    s.validations,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .labelSmall,
                                                  ),
                                                ),
                                                const BlankSpace(
                                                  onlyHorizontal: true,
                                                  times: 0.3,
                                                ),
                                                Align(
                                                  alignment:
                                                      Alignment.bottomCenter,
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      color: Theme.of(context)
                                                          .colorScheme
                                                          .primary,
                                                    ),
                                                    width: BlankSize.getSize(
                                                      context,
                                                      times: 0.5,
                                                    ),
                                                    height: BlankSize.getSize(
                                                      context,
                                                      times: 0.5,
                                                    ),
                                                  ),
                                                ),
                                                const BlankSpace(
                                                  onlyHorizontal: true,
                                                ),
                                                Baseline(
                                                  baseline: BlankSize.getSize(
                                                    context,
                                                    times: 0.5,
                                                  ),
                                                  baselineType:
                                                      TextBaseline.alphabetic,
                                                  child: Text(
                                                    s.resistance,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .labelSmall,
                                                  ),
                                                ),
                                                const BlankSpace(
                                                  onlyHorizontal: true,
                                                  times: 0.3,
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(
                                                    color: Theme.of(
                                                      context,
                                                    ).colorScheme.onSurface,
                                                  ),
                                                  width: BlankSize.getSize(
                                                    context,
                                                    times: 0.5,
                                                  ),
                                                  height: BlankSize.getSize(
                                                    context,
                                                    times: 0.5,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    );

                                  case StatsLoadingError:
                                    return ErrorWarningWidget(
                                      message: state.failure!.message,
                                    );
                                  default:
                                    return const Center(
                                      child: SizedBox(
                                        height: 60,
                                        width: 60,
                                        child: CircularWaitingWidget(),
                                      ),
                                    );
                                }
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
