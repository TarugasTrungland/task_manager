import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_manager/core/errors/failure.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habits_line_char_data.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_mini_habit_line_char_data.dart';

part 'stats_event.dart';
part 'stats_state.dart';

class StatsBloc extends Bloc<StatsEvent, StatsState> {
  StatsBloc({required GetMiniHabitLineCharData getMiniHabitLineCharData})
      : _getMiniHabitLineCharData = getMiniHabitLineCharData,
        super(const StatsInitial()) {
    on<StatsInit>(_statsInit);
    on<StatsLoad>(_loadingStats);
  }
  final GetMiniHabitLineCharData _getMiniHabitLineCharData;

  FutureOr<void> _statsInit(StatsInit event, Emitter<StatsState> emit) {
    emit(const StatsInitial());
  }

  FutureOr<void> _loadingStats(
    StatsLoad event,
    Emitter<StatsState> emit,
  ) async {
    final data = await _getMiniHabitLineCharData();
    data.fold(
      (failure) {
        emit(
          StatsLoadingError(
            miniHabitsStat: const MiniHabitsLineChartData.empty(),
            failure: failure,
          ),
        );
      },
      (success) => emit(StatsLoaded(miniHabitsStat: success)),
    );
  }
}
