part of 'stats_bloc.dart';

sealed class StatsState extends Equatable {
  const StatsState({required this.miniHabitsStat, this.failure});
  final MiniHabitsLineChartData miniHabitsStat;
  final Failure? failure;

  @override
  List<Object> get props => [miniHabitsStat];
}

final class StatsInitial extends StatsState {
  const StatsInitial()
      : super(
          miniHabitsStat: const MiniHabitsLineChartData.empty(),
        );
}

final class StatsLoaded extends StatsState {
  const StatsLoaded({required super.miniHabitsStat});
}

final class StatsLoadingError extends StatsState {
  const StatsLoadingError({
    required super.miniHabitsStat,
    required super.failure,
  });
}
