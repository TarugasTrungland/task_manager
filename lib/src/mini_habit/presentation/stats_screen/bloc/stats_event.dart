part of 'stats_bloc.dart';

sealed class StatsEvent {
  const StatsEvent();
}

class StatsInit extends StatsEvent {
  const StatsInit();
}

class StatsLoad extends StatsEvent {
  const StatsLoad();
}
