part of 'mini_habit_edit_bloc.dart';

sealed class MiniHabitEditEvent {
  const MiniHabitEditEvent();
}

class LoadMiniHabitsInEdit extends MiniHabitEditEvent {
  const LoadMiniHabitsInEdit();
}
