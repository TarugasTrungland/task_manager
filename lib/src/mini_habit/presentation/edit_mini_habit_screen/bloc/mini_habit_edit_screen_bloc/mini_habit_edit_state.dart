part of 'mini_habit_edit_bloc.dart';

sealed class MiniHabitEditState extends Equatable {
  const MiniHabitEditState({
    this.miniHabits = const [],
    this.errorMessage,
  });
  final List<MiniHabitEntity> miniHabits;
  final String? errorMessage;
  @override
  List<Object> get props => [miniHabits];
}

final class MiniHabitEditInitial extends MiniHabitEditState {
  MiniHabitEditInitial() : super(miniHabits: []);
}

class MiniHabitInEditLoaded extends MiniHabitEditState {
  const MiniHabitInEditLoaded({required super.miniHabits});
}

class MiniHabitInEditError extends MiniHabitEditState {
  const MiniHabitInEditError({required super.errorMessage});
}
