import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_all_active_minihabits.dart';

part 'mini_habit_edit_event.dart';
part 'mini_habit_edit_state.dart';

class MiniHabitEditBloc extends Bloc<MiniHabitEditEvent, MiniHabitEditState> {
  MiniHabitEditBloc({
    required GetAllActiveMiniHabits getAllActiveMiniHabits,
  })  : _getAllActiveMiniHabits = getAllActiveMiniHabits,
        super(MiniHabitEditInitial()) {
    on<LoadMiniHabitsInEdit>(_loadMiniHabitsHandler);
  }

  final GetAllActiveMiniHabits _getAllActiveMiniHabits;

  FutureOr<void> _loadMiniHabitsHandler(
    LoadMiniHabitsInEdit event,
    Emitter<MiniHabitEditState> emit,
  ) async {
    final result = await _getAllActiveMiniHabits();
    result.fold(
      (failure) =>
          emit(MiniHabitInEditError(errorMessage: failure.errorMessage)),
      (success) => emit(MiniHabitInEditLoaded(miniHabits: success)),
    );
  }
}
