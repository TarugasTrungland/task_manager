import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/src/mini_habit/presentation/edit_mini_habit_screen/bloc/mini_habit_edit_screen_bloc/mini_habit_edit_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/edit_mini_habit_screen/mini_habit_button.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_form/mini_habit_form_screen.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/error_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/mini_habit_tile.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/vertical_menu_widget.dart';

class EditMiniHabitsScreen extends StatelessWidget {
  const EditMiniHabitsScreen({super.key});
  static const routeName = 'edit_mini_habits';
  @override
  Widget build(BuildContext context) {
    final s = S.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          const Row(
            children: [
              Spacer(),
              VerticalMenuWidget(miniHabitScreen: MiniHabitScreen.edit),
            ],
          ),
          MiniHabitBackground(
            fileName: 'edit',
            child: Column(
              children: [
                SizedBox(
                  height: BlankSize.getSize(context, times: 3),
                ),
                Stack(
                  children: [
                    Text(
                      s.edit,
                      style: Theme.of(context).textTheme.displaySmall?.copyWith(
                            color: Theme.of(context).colorScheme.onSurface,
                            shadows: HardShadow.getShadow(context),
                          ),
                    ),
                    Center(
                      child: Column(
                        children: [
                          SizedBox(
                            height: BlankSize.getSize(context, times: 1.5),
                          ),
                          BlocBuilder<MiniHabitEditBloc, MiniHabitEditState>(
                            builder: (context, state) {
                              context
                                  .read<MiniHabitEditBloc>()
                                  .add(const LoadMiniHabitsInEdit());
                              switch (state.runtimeType) {
                                case MiniHabitEditInitial:
                                  return Container(
                                    height: 10,
                                    width: 100,
                                    color: Colors.red,
                                    child: const Text('loading'),
                                  );
                                case MiniHabitInEditLoaded:
                                  return ListView.builder(
                                    padding: EdgeInsets.zero,
                                    shrinkWrap: true,
                                    itemBuilder:
                                        (BuildContext context, int index) =>
                                            MiniHabitButton(
                                      onTap: () => Navigator.pushNamed(
                                        context,
                                        MiniHabitFormScreen.routeName,
                                        arguments: state.miniHabits[index].id,
                                      ),
                                      child: MiniHabitTile(
                                        miniHabit: state.miniHabits[index],
                                      ),
                                    ),
                                    itemCount: state.miniHabits.length,
                                  );
                                case MiniHabitInEditError:
                                  return ErrorWarningWidget(
                                    message: state.errorMessage,
                                  );
                                default:
                                  return const Center(
                                    child: Column(
                                      children: [Text('no state defined')],
                                    ),
                                  );
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
