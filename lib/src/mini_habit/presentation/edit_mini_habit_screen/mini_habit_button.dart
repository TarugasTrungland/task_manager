import 'package:flutter/material.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class MiniHabitButton extends StatelessWidget {
  const MiniHabitButton({required this.onTap, required this.child, super.key});
  final Widget child;
  final VoidCallback onTap;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: BlankSize.getSize(context),
      ),
      child: Material(
        color: Theme.of(context).colorScheme.surface,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
          side: BorderSide(
            color: Theme.of(context).colorScheme.onSurface,
          ),
        ),
        child: InkWell(
          splashColor: Theme.of(context).colorScheme.primary,
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: BlankSize.getSize(context),
            ),
            child: child,
          ),
        ),
      ),
    );
  }
}
