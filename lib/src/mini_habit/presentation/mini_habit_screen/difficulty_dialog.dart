import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/widgets/difficulty_selector_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/mini_habit_tile.dart';

class DifficultyDialog extends StatelessWidget {
  const DifficultyDialog({required this.miniHabit, super.key});
  final MiniHabitEntity miniHabit;
  @override
  Widget build(BuildContext context) {
    final s = S.of(context);
    return Dialog(
      insetPadding: EdgeInsets.all(BlankSize.getSize(context)),
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Theme.of(context).colorScheme.onSurface),
        borderRadius: BorderRadius.circular(0),
      ),
      elevation: 0,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: BlankSize.getSize(context, times: 2),
          vertical: BlankSize.getSize(context, times: 3),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            MiniHabitTile(
              miniHabit: miniHabit,
              hasBottomSleepingLine: true,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: BlankSize.getSize(context, times: 2),
                bottom: BlankSize.getSize(context, times: 4),
              ),
              child: const ResistanceSelectorWidget(),
            ),
            ElevatedButton(
              onPressed: () => Navigator.pop(context),
              child: Text(s.cancel_button.toUpperCase()),
            ),
          ],
        ),
      ),
    );
  }
}
