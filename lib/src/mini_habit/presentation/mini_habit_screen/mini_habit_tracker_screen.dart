import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/bloc/user_bloc/user_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/widgets/header_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/widgets/mini_habit_tracker_list_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/circular_waiting_widget.dart';

class MiniHabitTrackerScreen extends StatelessWidget {
  const MiniHabitTrackerScreen({super.key});

  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          switch (state.runtimeType) {
            case UserInit:
              return const Center(
                child: CircularWaitingWidget(),
              );
            case DailyQuoteLoaded:
              return (state.dailyQuote != null)
                  ? Column(
                      children: [
                        HeaderWidget(
                          quote: state.dailyQuote!.quote!,
                          author: state.dailyQuote!.author!,
                        ),
                        const MiniHabitBackground(
                          child: MiniHabitTrackerListWidget(),
                        ),
                      ],
                    )
                  : const SizedBox();
            default:
              return const Center(
                child: Column(
                  children: [Text('no user state defined')],
                ),
              );
          }
        },
      ),
    );
  }
}
