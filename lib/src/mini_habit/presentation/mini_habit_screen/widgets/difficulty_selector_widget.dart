import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class ResistanceSelectorWidget extends StatelessWidget {
  const ResistanceSelectorWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final s = S.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(s.difficulty, style: Theme.of(context).textTheme.titleLarge),
            IconButton(
              onPressed: () => debugPrint('show explanation'),
              icon: Icon(
                Icons.info_outline,
                color: Theme.of(context).colorScheme.tertiary,
              ),
            ),
          ],
        ),
        const BlankSpace(onlyVertical: true),
        SizedBox(
          height: BlankSize.getSize(context, times: 5),
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ResistanceButton(difficulty: 4),
              BlankSpace(onlyHorizontal: true),
              ResistanceButton(difficulty: 3),
              BlankSpace(onlyHorizontal: true),
              ResistanceButton(difficulty: 2),
              BlankSpace(onlyHorizontal: true),
              ResistanceButton(difficulty: 1),
            ],
          ),
        ),
      ],
    );
  }
}

class ResistanceButton extends StatelessWidget {
  const ResistanceButton({required this.difficulty, super.key});
  final int difficulty;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: () => Navigator.pop(context, difficulty),
        child: Container(
          height: BlankSize.getSize(context, times: 5),
          decoration: BoxDecoration(
            border: Border.all(),
            borderRadius: const BorderRadius.all(Radius.circular(4)),
          ),
          child: Padding(
            padding: EdgeInsets.all(BlankSize.getSize(context)),
            child: SvgPicture.asset(
              'assets/images/difficulty_icons/$difficulty.svg',
              width: 60,
              height: 24,
            ),
          ),
        ),
      ),
    );
  }
}
