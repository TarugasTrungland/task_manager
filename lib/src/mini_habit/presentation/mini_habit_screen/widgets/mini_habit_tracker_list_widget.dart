import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/bloc/mini_habit_tracker_screen_bloc/mini_habit_bloc.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/difficulty_dialog.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/widgets/empty_mini_habits_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/circular_waiting_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/error_widget.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/mini_habit_tile.dart';

class MiniHabitTrackerListWidget extends StatefulWidget {
  const MiniHabitTrackerListWidget({super.key});

  @override
  State<MiniHabitTrackerListWidget> createState() =>
      _MiniHabitTrackerListWidgetState();
}

class _MiniHabitTrackerListWidgetState
    extends State<MiniHabitTrackerListWidget> {
  @override
  void initState() {
    super.initState();
    context.read<MiniHabitBloc>().add(const LoadMiniHabits());
  }

  @override
  Widget build(BuildContext context) {
    final s = S.of(context);
    return BlocBuilder<MiniHabitBloc, MiniHabitState>(
      builder: (context, state) {
        switch (state.runtimeType) {
          case LoadingMiniHabits:
            return const Center(
              child: CircularWaitingWidget(),
            );
          case MiniHabitLoaded:
            return Padding(
              padding:
                  EdgeInsets.only(top: BlankSize.getSize(context, times: 3)),
              child: state.miniHabits.isEmpty
                  ? const EmptyMiniHabitsWidget()
                  : Column(
                      children: [
                        Stack(
                          children: [
                            Text(
                              s.today,
                              style: Theme.of(context)
                                  .textTheme
                                  .displaySmall
                                  ?.copyWith(
                                    color:
                                        Theme.of(context).colorScheme.onSurface,
                                    shadows: HardShadow.getShadow(context),
                                  ),
                            ),
                            Center(
                              child: Column(
                                children: [
                                  SizedBox(
                                    height:
                                        BlankSize.getSize(context, times: 2.5),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color:
                                          Theme.of(context).colorScheme.surface,
                                      border: Border.all(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onSurface,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(12),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.all(
                                        BlankSize.getSize(context),
                                      ),
                                      child: ListView.builder(
                                        padding: EdgeInsets.zero,
                                        shrinkWrap: true,
                                        itemBuilder:
                                            (BuildContext context, int index) =>
                                                Material(
                                          child: InkWell(
                                            splashColor: Theme.of(context)
                                                .colorScheme
                                                .primary,
                                            onTap: () async {
                                              final result =
                                                  await showDialog<int>(
                                                barrierColor:
                                                    const Color(0x01000000),
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return DifficultyDialog(
                                                    miniHabit:
                                                        state.miniHabits[index],
                                                  );
                                                },
                                              );

                                              if (result != null &&
                                                  context.mounted) {
                                                context
                                                    .read<MiniHabitBloc>()
                                                    .add(
                                                      ValidateMiniHabitEvent(
                                                        id: state
                                                            .miniHabits[index]
                                                            .id!,
                                                        resistance: result,
                                                      ),
                                                    );
                                              }
                                            },
                                            child: MiniHabitTile(
                                              miniHabit:
                                                  state.miniHabits[index],
                                              hasBottomSleepingLine: index !=
                                                  state.miniHabits.length - 1,
                                            ),
                                          ),
                                        ),
                                        itemCount: state.miniHabits.length,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
            );

          case OnMiniHabitError:
            return Center(
              child: ErrorWarningWidget(message: state.errorMessage),
            );

          default:
            return const Center(
              child: ErrorWarningWidget(),
            );
        }
      },
    );
  }
}
