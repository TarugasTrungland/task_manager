import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:lottie/lottie.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class EmptyMiniHabitsWidget extends StatelessWidget {
  const EmptyMiniHabitsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final s = S.of(context);
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        border: Border.all(color: Theme.of(context).colorScheme.onSurface),
      ),
      child: Padding(
        padding: EdgeInsets.all(BlankSize.getSize(context, times: 2)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              s.congratulations,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const BlankSpace(
              times: 2,
            ),
            Text(
              s.all_mini_habits_validated,
              style: Theme.of(context).textTheme.bodyLarge,
              textAlign: TextAlign.center,
            ),
            const BlankSpace(),
            SizedBox(
              height: BlankSize.getSize(context, times: 12),
              width: BlankSize.getSize(context, times: 12),
              child: Lottie.asset(
                'assets/animations/all_evaluated.json',
                repeat: false,
              ),
            ),
            const BlankSpace(),
            Text(
              s.more_tomorrow,
              style: Theme.of(context).textTheme.bodyLarge,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
