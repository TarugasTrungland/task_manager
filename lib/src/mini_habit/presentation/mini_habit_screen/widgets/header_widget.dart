import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';
import 'package:task_manager/src/mini_habit/presentation/widgets/vertical_menu_widget.dart';

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({
    required this.quote,
    required this.author,
    super.key,
  });
  final String quote;
  final String author;
  @override
  Widget build(BuildContext context) {
    final s = S.of(context);
    return Padding(
      padding: EdgeInsets.only(
        left: BlankSize.getSize(context, times: 2),
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: SafeArea(
                  child: Padding(
                    padding: EdgeInsets.only(
                      right: BlankSize.getSize(context),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const BlankSpace(
                          onlyVertical: true,
                          times: 2,
                        ),
                        Text(
                          s.hello,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                        Text(
                          quote,
                          maxLines: 4,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.labelMedium,
                        ),
                        Text(
                          author,
                          style: Theme.of(context)
                              .textTheme
                              .labelMedium
                              ?.copyWith(fontStyle: FontStyle.italic),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const VerticalMenuWidget(miniHabitScreen: MiniHabitScreen.home),
            ],
          ),
        ],
      ),
    );
  }
}
