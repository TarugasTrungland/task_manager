import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_all_active_minihabits.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/validate_minihabit.dart';

part 'mini_habit_event.dart';
part 'mini_habit_state.dart';

class MiniHabitBloc extends Bloc<MiniHabitEvent, MiniHabitState> {
  MiniHabitBloc({
    required ValidateMiniHabit validateMiniHabit,
    required GetAllActiveMiniHabits getAllActiveMiniHabits,
  })  : _getAllActiveMiniHabits = getAllActiveMiniHabits,
        _validateMiniHabit = validateMiniHabit,
        super(LoadingMiniHabits()) {
    on<LoadMiniHabits>(_loadMiniHabitsHandler);
    on<ValidateMiniHabitEvent>(_validateMiniHabitHandler);
  }

  final GetAllActiveMiniHabits _getAllActiveMiniHabits;
  final ValidateMiniHabit _validateMiniHabit;

  Future<void> _loadMiniHabitsHandler(
    MiniHabitEvent event,
    Emitter<MiniHabitState> emit,
  ) async {
    final result = await _getAllActiveMiniHabits();
    result.fold(
      (failure) => emit(
        OnMiniHabitError(
          errorMessage: failure.errorMessage,
          miniHabits: const [],
        ),
      ),
      (result) => emit(
        MiniHabitLoaded(
          miniHabits: result,
        ),
      ),
    );
  }

  Future<void> _validateMiniHabitHandler(
    ValidateMiniHabitEvent event,
    Emitter<MiniHabitState> emit,
  ) async {
    final result = await _validateMiniHabit(
      ValidateMiniHabitParams(id: event.id, resistance: event.resistance),
    );
    result.fold(
      (failure) => emit(
        OnMiniHabitError(
          errorMessage: failure.errorMessage,
          miniHabits: const [],
        ),
      ),
      (_) {
        emit(LoadingMiniHabits());
        add(const LoadMiniHabits());
      },
    );
  }
}
