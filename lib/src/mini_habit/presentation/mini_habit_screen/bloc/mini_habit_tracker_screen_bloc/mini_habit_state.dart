part of 'mini_habit_bloc.dart';

sealed class MiniHabitState extends Equatable {
  const MiniHabitState({required this.miniHabits, this.errorMessage});
  final List<MiniHabitEntity> miniHabits;
  final String? errorMessage;
  @override
  List<Object> get props => [miniHabits];
}

class LoadingMiniHabits extends MiniHabitState {
  LoadingMiniHabits() : super(miniHabits: []);
}

class MiniHabitLoaded extends MiniHabitState {
  const MiniHabitLoaded({required super.miniHabits});
}

class OnMiniHabitError extends MiniHabitState {
  const OnMiniHabitError({
    required super.errorMessage,
    required super.miniHabits,
  });
}
