part of 'mini_habit_bloc.dart';

abstract class MiniHabitEvent {
  const MiniHabitEvent();
}

class LoadMiniHabits extends MiniHabitEvent {
  const LoadMiniHabits();
}

class ValidateMiniHabitEvent extends MiniHabitEvent {
  const ValidateMiniHabitEvent({required this.id, required this.resistance});
  final int id;
  final int resistance;
}
