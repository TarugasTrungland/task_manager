part of 'user_bloc.dart';

@immutable
abstract class UserEvent extends Equatable {
  const UserEvent();

  @override
  List<Object?> get props => [];
}

class Init extends UserEvent {
  const Init(this.locale);
  final Locale locale;
}
