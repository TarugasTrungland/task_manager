part of 'user_bloc.dart';

@immutable
sealed class UserState {
  const UserState({required this.dailyQuote});
  final DailyQuoteEntity? dailyQuote;
}

final class UserInit extends UserState {
  const UserInit() : super(dailyQuote: null);
}

final class DailyQuoteLoaded extends UserState {
  const DailyQuoteLoaded({required super.dailyQuote});
}
