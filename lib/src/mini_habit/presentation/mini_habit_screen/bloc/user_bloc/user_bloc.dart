import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:task_manager/src/mini_habit/domain/entities/daily_quote_entity.dart';
import 'package:task_manager/src/mini_habit/domain/usecases/get_quote_of_the_day.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc({required GetQuoteOfTheDay getQuoteOfTheDay})
      : _getQuoteOfTheDay = getQuoteOfTheDay,
        super(const UserInit()) {
    on<Init>(_onInitUser);
  }

  final GetQuoteOfTheDay _getQuoteOfTheDay;
  Future<void> _onInitUser(Init event, Emitter<UserState> emit) async {
    final dailyQuote = await _getQuoteOfTheDay.call();
    dailyQuote.fold(
      (failure) => print('error'),
      (success) => emit(
        DailyQuoteLoaded(
          dailyQuote: DailyQuoteEntity(
            quote: success.quote,
            author: success.author,
          ),
        ),
      ),
    );
  }
}
