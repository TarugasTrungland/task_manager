import 'package:flutter/material.dart';
import 'package:task_manager/src/mini_habit/presentation/edit_mini_habit_screen/edit_mini_habit_screen.dart';
import 'package:task_manager/src/mini_habit/presentation/mini_habit_screen/mini_habit_tracker_screen.dart';
import 'package:task_manager/src/mini_habit/presentation/stats_screen/stats_screen.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

enum MiniHabitScreen {
  home,
  edit,
  stats,
}

extension MiniHabitScreenExtension on MiniHabitScreen {
  IconData get icon {
    switch (this) {
      case MiniHabitScreen.home:
        return Icons.home_outlined;
      case MiniHabitScreen.edit:
        return Icons.tune;
      case MiniHabitScreen.stats:
        return Icons.query_stats;
    }
  }

  String get route {
    switch (this) {
      case MiniHabitScreen.home:
        return MiniHabitTrackerScreen.routeName;
      case MiniHabitScreen.edit:
        return EditMiniHabitsScreen.routeName;
      case MiniHabitScreen.stats:
        return StatsScreen.routeName;
    }
  }
}

class VerticalMenuWidget extends StatelessWidget {
  const VerticalMenuWidget({
    required this.miniHabitScreen,
    super.key,
  });
  final MiniHabitScreen miniHabitScreen;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.primary,
      ),
      child: Padding(
        padding: EdgeInsets.all(BlankSize.getSize(context)),
        child: SafeArea(
          child: Column(
            children: [
              _iconButton(
                context,
                current: miniHabitScreen,
                goTo: MiniHabitScreen.home,
              ),
              _iconButton(
                context,
                current: miniHabitScreen,
                goTo: MiniHabitScreen.stats,
              ),
              _iconButton(
                context,
                current: miniHabitScreen,
                goTo: MiniHabitScreen.edit,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _iconButton(
    BuildContext context, {
    required MiniHabitScreen current,
    required MiniHabitScreen goTo,
  }) {
    return (current == goTo)
        ? Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(6)),
              border: Border.all(
                color: Theme.of(context).colorScheme.surface,
              ),
            ),
            height: 50,
            width: 50,
            child: Icon(
              goTo.icon,
              color: Theme.of(context).colorScheme.surface,
            ),
          )
        : IconButton(
            icon: Icon(goTo.icon),
            onPressed: () => Navigator.pushNamed(context, goTo.route),
          );
  }
}
