import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class CircularWaitingWidget extends StatelessWidget {
  const CircularWaitingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: BlankSize.getSize(context, times: 2),
      width: BlankSize.getSize(context, times: 2),
      child: Lottie.asset('assets/animations/circular_waiting.json'),
    );
  }
}
