import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:lottie/lottie.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class ErrorWarningWidget extends StatelessWidget {
  const ErrorWarningWidget({this.message, super.key});
  final String? message;
  @override
  Widget build(BuildContext context) {
    final s = S.of(context);

    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        border: Border.all(color: Theme.of(context).colorScheme.onSurface),
      ),
      child: Padding(
        padding: EdgeInsets.all(BlankSize.getSize(context, times: 2)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              s.error,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const BlankSpace(),
            SizedBox(
              height: BlankSize.getSize(context, times: 18),
              width: BlankSize.getSize(context, times: 18),
              child: Lottie.asset(
                'assets/animations/warning_animation.json',
                repeat: false,
              ),
            ),
            const BlankSpace(),
            Text(
              message ?? s.not_defined_error,
              style: Theme.of(context).textTheme.bodyLarge,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
