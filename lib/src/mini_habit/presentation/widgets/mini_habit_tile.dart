import 'package:flutter/material.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';

import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class MiniHabitTile extends StatelessWidget {
  const MiniHabitTile({
    required this.miniHabit,
    this.hasBottomSleepingLine = false,
    super.key,
  });
  final MiniHabitEntity miniHabit;
  final bool hasBottomSleepingLine;

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'MHkey_${miniHabit.id}',
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: BlankSize.getSize(context, times: 6),
        ),
        child: Container(
          decoration:
              BoxDecoration(color: Theme.of(context).colorScheme.surface),
          child: Column(
            children: [
              SizedBox(
                height: BlankSize.getSize(context),
              ),
              Row(
                children: [
                  SizedBox(
                    width: BlankSize.getSize(context, times: 4),
                    child: const Icon(Icons.access_alarm),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      right: BlankSize.getSize(context, times: 4),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          miniHabit.category?.title ?? ' ',
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                        Text(
                          miniHabit.title ?? ' ',
                          style: Theme.of(context).textTheme.labelMedium,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: BlankSize.getSize(context),
              ),
              if (hasBottomSleepingLine)
                Container(
                  height: 1,
                  width: double.infinity,
                  color: Theme.of(context).colorScheme.onSurface,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
