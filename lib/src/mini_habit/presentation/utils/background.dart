import 'package:flutter/material.dart';
import 'package:task_manager/src/mini_habit/presentation/utils/ui_utils.dart';

class MiniHabitBackground extends StatelessWidget {
  const MiniHabitBackground({
    required this.child,
    this.hasTopSleepingLine = true,
    this.hasRepeatedImage = true,
    this.fileName = 'bg',
    super.key,
  });
  final Widget child;
  final bool hasTopSleepingLine;
  final bool hasRepeatedImage;
  final String fileName;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: [
          const SizedBox.expand(),
          Positioned(
            top: 0,
            left: 0,
            bottom: 0,
            child: Container(
              width: BlankSize.getSize(context, times: 8),
              color: Theme.of(context).colorScheme.primary,
            ),
          ),
          if (hasRepeatedImage)
            Positioned.fill(
              child: Image.asset(
                'assets/images/background/$fileName.png',
                repeat: ImageRepeat.repeat,
              ),
            ),
          if (hasTopSleepingLine)
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 1,
                color: Theme.of(context).colorScheme.onSurface,
              ),
            ),
          Padding(
            padding: EdgeInsets.only(
              left: BlankSize.getSize(context, times: 2),
              right: BlankSize.getSize(context, times: 2),
            ),
            child: child,
          ),
        ],
      ),
    );
  }
}
