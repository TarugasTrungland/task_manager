import 'package:flutter/material.dart';

class HardShadow {
  static List<Shadow> getShadow(BuildContext context) {
    return [
      Shadow(
          color: Theme.of(context).colorScheme.surface,
          offset: const Offset(-1, 1),),
    ];
  }

  static List<BoxShadow> getBoxShadow(BuildContext context) {
    return [
      BoxShadow(
          color: Theme.of(context).colorScheme.surface,
          offset: const Offset(-1, 1),),
    ];
  }
}
