import 'package:flutter/material.dart';

class BlankSpace extends StatelessWidget {
  const BlankSpace(
      {this.times = 1.0,
      this.onlyHorizontal = false,
      this.onlyVertical = false,
      super.key,});
  final bool onlyVertical;
  final bool onlyHorizontal;
  final double times;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: onlyVertical
          ? 0
          : ((Theme.of(context).textTheme.bodySmall?.fontSize ?? 12) * times)
              .roundToDouble(),
      height: onlyHorizontal
          ? 0
          : ((Theme.of(context).textTheme.bodySmall?.fontSize ?? 12) * times)
              .roundToDouble(),
    );
  }
}

class BlankSize {
  static double getSize(BuildContext context, {double times = 1}) =>
      ((Theme.of(context).textTheme.bodySmall?.fontSize ?? 12) * times)
          .roundToDouble();
}
