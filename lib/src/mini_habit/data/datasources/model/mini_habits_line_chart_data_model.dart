import 'package:equatable/equatable.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habits_line_char_data.dart';

class MiniHabitsLineChartDataModel extends MiniHabitsLineChartData
    with EquatableMixin {
  const MiniHabitsLineChartDataModel({
    required super.dayNumbers,
    required super.flSpots,
    required super.resistanceFlSpots,
  });

  const MiniHabitsLineChartDataModel.empty()
      : this(
          dayNumbers: const [0],
          flSpots: const [FlSpot(1, 1)],
          resistanceFlSpots: const [FlSpot(2, 2)],
        );

  MiniHabitsLineChartData toEntity() => MiniHabitsLineChartData(
        dayNumbers: dayNumbers,
        flSpots: flSpots,
        resistanceFlSpots: resistanceFlSpots,
      );
  factory MiniHabitsLineChartDataModel.fromData({
    required List<int> count,
    required DateTime lastDay,
    required List<double> resistances,
  }) {
    final countList = <FlSpot>[];
    var countIndex = count.length - 1;
    count.map((validation) {
      countList.add(FlSpot(countIndex.toDouble(), validation.toDouble()));
      countIndex--;
    }).toList();

    final resistanceList = <FlSpot>[];
    var resistanceIndex = resistances.length - 1;
    resistances.map((validation) {
      resistanceList.add(FlSpot(resistanceIndex.toDouble(), validation));
      resistanceIndex--;
    }).toList();
    final dayList = <int>[];
    count.asMap().keys.map((k) {
      final datetime = DateTime(
        lastDay.year,
        lastDay.month,
        lastDay.day - count.length + 1 + k,
      );
      dayList.add(datetime.day);
    }).toList();

    return MiniHabitsLineChartDataModel(
      dayNumbers: dayList,
      flSpots: countList,
      resistanceFlSpots: resistanceList,
    );
  }
}
