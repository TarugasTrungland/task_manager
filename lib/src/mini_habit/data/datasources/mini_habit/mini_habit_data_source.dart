import 'package:task_manager/src/mini_habit/data/datasources/_collections/export_datasources.dart';
import 'package:task_manager/src/mini_habit/data/datasources/model/mini_habits_line_chart_data_model.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';

abstract class MiniHabitDataSource {
  const MiniHabitDataSource();

  Future<List<MiniHabitCollection>> getAllActiveMiniHabits();
  Future<DailyQuoteCollection?> getQuoteOfTheDay();

  Future<void> validateMiniHabit({required int id, required int resistance});

  Future<MiniHabitCollection?> getMiniHabitById({required int id});

  Future<List<CategoryCollection>> getAllCategories();

  Future<CategoryCollection> createCategory({required String title});

  Future<void> updateMiniHabit({
    required MiniHabitEntity miniHabitEntity,
  });

  Future<MiniHabitsLineChartDataModel> getMiniHabitLineCharData();
}
