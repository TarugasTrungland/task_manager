import 'package:isar/isar.dart';
import 'package:task_manager/core/database/local_database.dart';
import 'package:task_manager/src/mini_habit/data/datasources/_collections/export_datasources.dart';
import 'package:task_manager/src/mini_habit/data/datasources/mini_habit/mini_habit_data_source.dart';
import 'package:task_manager/src/mini_habit/data/datasources/model/mini_habits_line_chart_data_model.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';

class MiniHabitDataSourceImpl extends MiniHabitDataSource {
  const MiniHabitDataSourceImpl(this._localDatabase);

  final LocalDatabase _localDatabase;

  DateTime _day(int fromToday) {
    final now = DateTime.now();
    return DateTime(now.year, now.month, now.day + fromToday);
  }

  @override
  Future<List<MiniHabitCollection>> getAllActiveMiniHabits() async {
    try {
      final db = _localDatabase.db;
      final all = await db.miniHabitCollections.where().findAll();
      final today = await db.miniHabitCollections
          .filter()
          .validations((q) => q.dateTimeBetween(_day(0), _day(1)))
          .findAll();
      final idsWithValidations = today.map((miniHabit) => miniHabit.id).toSet();
      return all
          .where(
            (miniHabitCollection) =>
                !idsWithValidations.contains(miniHabitCollection.id),
          )
          .toList();
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<DailyQuoteCollection?> getQuoteOfTheDay() async {
    final db = _localDatabase.db;
    final today = _day(0);
    return db.dailyQuoteCollections.filter().dateEqualTo(today).findFirst();
  }

  @override
  Future<void> validateMiniHabit({
    required int id,
    required int resistance,
  }) async {
    try {
      final db = _localDatabase.db;
      final miniHabit = await db.miniHabitCollections.get(id);
      if (miniHabit != null) {
        final validation = ValidationCollection()
          ..dateTime = DateTime.now()
          ..resistance = resistance
          ..miniHabit.value = miniHabit;
        await db.writeTxn(() async {
          await db.validationCollections.put(validation);
          await validation.miniHabit.save();
        });
      }
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<MiniHabitCollection?> getMiniHabitById({required int id}) async {
    try {
      final db = _localDatabase.db;
      return db.miniHabitCollections.get(id);
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<List<CategoryCollection>> getAllCategories() async {
    final db = _localDatabase.db;
    return db.categoryCollections.where().findAll();
  }

  @override
  Future<CategoryCollection> createCategory({required String title}) async {
    try {
      final db = _localDatabase.db;
      final newCategory = CategoryCollection()..title = title;

      await db.writeTxn(() async {
        await db.categoryCollections.put(newCategory);
      });
      return newCategory;
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<void> updateMiniHabit({
    required MiniHabitEntity miniHabitEntity,
  }) async {
    try {
      final db = _localDatabase.db;
      final miniHabit = await getMiniHabitById(id: miniHabitEntity.id!);
      final category =
          await db.categoryCollections.get(miniHabitEntity.category!.id!);
      await db.writeTxn(() async {
        miniHabit?.title = miniHabitEntity.title;
        if (category != null) {
          miniHabit?.category.value = category;
          await miniHabit?.category.save();
        }
        await db.miniHabitCollections.put(miniHabit!);
      });
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<MiniHabitsLineChartDataModel> getMiniHabitLineCharData() async {
    final data = <int>[];
    final resistanceData = <double>[];
    final db = _localDatabase.db;
    var lastMean = 0.0;
    const numberOfDays = 7;
    try {
      for (var i = 0; i < numberOfDays; i++) {
        final validationCount = await db.validationCollections
            .filter()
            .dateTimeBetween(_day(i * -1), _day((i * -1) + 1))
            .count();
        data.add(validationCount);
        final validations = await db.validationCollections
            .filter()
            .dateTimeBetween(_day(i * -1), _day((i * -1) + 1))
            .findAll();
        final resistances = validations
            .map((v) => (v.resistance == null) ? 0 : v.resistance!)
            .toList();
        if (resistances.isNotEmpty) {
          final totalResistance = resistances.reduce((a, b) => a + b);
          lastMean = totalResistance / resistances.length;
          resistanceData.add(totalResistance / resistances.length);
        } else {
          resistanceData.add(lastMean / 2);
          lastMean = lastMean / 2;
        }
      }

      return MiniHabitsLineChartDataModel.fromData(
        count: data,
        lastDay: _day(0),
        resistances: resistanceData,
      );
    } catch (_) {
      rethrow;
    }
  }
}
