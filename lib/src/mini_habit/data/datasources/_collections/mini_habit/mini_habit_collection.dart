import 'package:isar/isar.dart';
import 'package:task_manager/src/mini_habit/data/datasources/_collections/category/category_collection.dart';
import 'package:task_manager/src/mini_habit/data/datasources/_collections/validation/validation_collection.dart';
import 'package:task_manager/src/mini_habit/data/datasources/mappers/entity_convertible.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';

part 'mini_habit_collection.g.dart';

// minihabit
@collection
class MiniHabitCollection
    with EntityConvertible<MiniHabitCollection, MiniHabitEntity> {
  MiniHabitCollection({this.title, this.description, this.id});

  MiniHabitCollection.empty()
      : this(id: 0, title: '_empty.title', description: '_empty.description');

  Id? id;
  String? title;
  String? description;
  final category = IsarLink<CategoryCollection>();
  final validations = IsarLinks<ValidationCollection>();

  @override
  MiniHabitCollection fromEntity(MiniHabitEntity model) => MiniHabitCollection(
        id: model.id,
        title: model.title,
        description: model.description,
      );

  @override
  MiniHabitEntity toEntity() => MiniHabitEntity(
        id: id,
        title: title,
        description: description,
        category: category.value?.toEntity(),
      );
}
