import 'package:isar/isar.dart';
import 'package:task_manager/src/mini_habit/data/datasources/mappers/entity_convertible.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';

part 'category_collection.g.dart';

// category of minihabit
@Collection()
class CategoryCollection
    with EntityConvertible<CategoryCollection, CategoryEntity> {
  CategoryCollection({this.id, this.title});

  CategoryCollection.empty() : this(id: 0, title: '_empty.title');

  Id? id;
  String? title;

  @override
  CategoryEntity toEntity() => CategoryEntity(
        id: id,
        title: title,
      );

  @override
  CategoryCollection fromEntity(CategoryEntity model) =>
      CategoryCollection(id: id, title: title);
}
