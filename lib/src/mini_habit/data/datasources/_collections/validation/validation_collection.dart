import 'package:isar/isar.dart';
import 'package:task_manager/src/mini_habit/data/datasources/_collections/mini_habit/mini_habit_collection.dart';
import 'package:task_manager/src/mini_habit/data/datasources/mappers/entity_convertible.dart';
import 'package:task_manager/src/mini_habit/domain/entities/validation_entity.dart';

part 'validation_collection.g.dart';

@Collection()
class ValidationCollection
    with EntityConvertible<ValidationCollection, ValidationEntity> {
  ValidationCollection({this.id, this.dateTime, this.resistance});
  Id? id;
  DateTime? dateTime;
  int? resistance;

  @Backlink(to: 'validations')
  final miniHabit = IsarLink<MiniHabitCollection>();

  @override
  ValidationEntity toEntity() =>
      ValidationEntity(id: id, dateTime: dateTime, resistance: resistance);

  @override
  ValidationCollection fromEntity(ValidationEntity model) =>
      ValidationCollection(
        id: model.id,
        dateTime: model.dateTime,
        resistance: model.resistance,
      );
}
