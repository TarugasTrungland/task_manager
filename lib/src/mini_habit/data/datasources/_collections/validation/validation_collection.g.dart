// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'validation_collection.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetValidationCollectionCollection on Isar {
  IsarCollection<ValidationCollection> get validationCollections =>
      this.collection();
}

const ValidationCollectionSchema = CollectionSchema(
  name: r'ValidationCollection',
  id: 6102545340143950055,
  properties: {
    r'dateTime': PropertySchema(
      id: 0,
      name: r'dateTime',
      type: IsarType.dateTime,
    ),
    r'resistance': PropertySchema(
      id: 1,
      name: r'resistance',
      type: IsarType.long,
    )
  },
  estimateSize: _validationCollectionEstimateSize,
  serialize: _validationCollectionSerialize,
  deserialize: _validationCollectionDeserialize,
  deserializeProp: _validationCollectionDeserializeProp,
  idName: r'id',
  indexes: {},
  links: {
    r'miniHabit': LinkSchema(
      id: 641406299568961762,
      name: r'miniHabit',
      target: r'MiniHabitCollection',
      single: true,
      linkName: r'validations',
    )
  },
  embeddedSchemas: {},
  getId: _validationCollectionGetId,
  getLinks: _validationCollectionGetLinks,
  attach: _validationCollectionAttach,
  version: '3.1.0+1',
);

int _validationCollectionEstimateSize(
  ValidationCollection object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  return bytesCount;
}

void _validationCollectionSerialize(
  ValidationCollection object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeDateTime(offsets[0], object.dateTime);
  writer.writeLong(offsets[1], object.resistance);
}

ValidationCollection _validationCollectionDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = ValidationCollection(
    dateTime: reader.readDateTimeOrNull(offsets[0]),
    id: id,
    resistance: reader.readLongOrNull(offsets[1]),
  );
  return object;
}

P _validationCollectionDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 1:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _validationCollectionGetId(ValidationCollection object) {
  return object.id ?? Isar.autoIncrement;
}

List<IsarLinkBase<dynamic>> _validationCollectionGetLinks(
    ValidationCollection object) {
  return [object.miniHabit];
}

void _validationCollectionAttach(
    IsarCollection<dynamic> col, Id id, ValidationCollection object) {
  object.id = id;
  object.miniHabit.attach(
      col, col.isar.collection<MiniHabitCollection>(), r'miniHabit', id);
}

extension ValidationCollectionQueryWhereSort
    on QueryBuilder<ValidationCollection, ValidationCollection, QWhere> {
  QueryBuilder<ValidationCollection, ValidationCollection, QAfterWhere>
      anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension ValidationCollectionQueryWhere
    on QueryBuilder<ValidationCollection, ValidationCollection, QWhereClause> {
  QueryBuilder<ValidationCollection, ValidationCollection, QAfterWhereClause>
      idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterWhereClause>
      idLessThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterWhereClause>
      idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension ValidationCollectionQueryFilter on QueryBuilder<ValidationCollection,
    ValidationCollection, QFilterCondition> {
  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> dateTimeIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dateTime',
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> dateTimeIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dateTime',
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> dateTimeEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> dateTimeGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> dateTimeLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dateTime',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> dateTimeBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dateTime',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> idIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> idIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'id',
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> idEqualTo(Id? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> idGreaterThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> idLessThan(
    Id? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> idBetween(
    Id? lower,
    Id? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> resistanceIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'resistance',
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> resistanceIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'resistance',
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> resistanceEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'resistance',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> resistanceGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'resistance',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> resistanceLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'resistance',
        value: value,
      ));
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> resistanceBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'resistance',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension ValidationCollectionQueryObject on QueryBuilder<ValidationCollection,
    ValidationCollection, QFilterCondition> {}

extension ValidationCollectionQueryLinks on QueryBuilder<ValidationCollection,
    ValidationCollection, QFilterCondition> {
  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> miniHabit(FilterQuery<MiniHabitCollection> q) {
    return QueryBuilder.apply(this, (query) {
      return query.link(q, r'miniHabit');
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection,
      QAfterFilterCondition> miniHabitIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.linkLength(r'miniHabit', 0, true, 0, true);
    });
  }
}

extension ValidationCollectionQuerySortBy
    on QueryBuilder<ValidationCollection, ValidationCollection, QSortBy> {
  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      sortByDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateTime', Sort.asc);
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      sortByDateTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateTime', Sort.desc);
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      sortByResistance() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'resistance', Sort.asc);
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      sortByResistanceDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'resistance', Sort.desc);
    });
  }
}

extension ValidationCollectionQuerySortThenBy
    on QueryBuilder<ValidationCollection, ValidationCollection, QSortThenBy> {
  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      thenByDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateTime', Sort.asc);
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      thenByDateTimeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'dateTime', Sort.desc);
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      thenByResistance() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'resistance', Sort.asc);
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QAfterSortBy>
      thenByResistanceDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'resistance', Sort.desc);
    });
  }
}

extension ValidationCollectionQueryWhereDistinct
    on QueryBuilder<ValidationCollection, ValidationCollection, QDistinct> {
  QueryBuilder<ValidationCollection, ValidationCollection, QDistinct>
      distinctByDateTime() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'dateTime');
    });
  }

  QueryBuilder<ValidationCollection, ValidationCollection, QDistinct>
      distinctByResistance() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'resistance');
    });
  }
}

extension ValidationCollectionQueryProperty on QueryBuilder<
    ValidationCollection, ValidationCollection, QQueryProperty> {
  QueryBuilder<ValidationCollection, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<ValidationCollection, DateTime?, QQueryOperations>
      dateTimeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'dateTime');
    });
  }

  QueryBuilder<ValidationCollection, int?, QQueryOperations>
      resistanceProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'resistance');
    });
  }
}
