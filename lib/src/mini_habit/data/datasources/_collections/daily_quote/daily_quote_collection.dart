import 'package:isar/isar.dart';
import 'package:task_manager/src/mini_habit/data/datasources/mappers/entity_convertible.dart';
import 'package:task_manager/src/mini_habit/domain/entities/daily_quote_entity.dart';

part 'daily_quote_collection.g.dart';

// daily quote
@Collection()
class DailyQuoteCollection
    with EntityConvertible<DailyQuoteCollection, DailyQuoteEntity> {
  DailyQuoteCollection({this.id, this.quote, this.author, this.date});
  DailyQuoteCollection.empty()
      : this(
          id: 0,
          quote: '_empty.quote',
          author: '_empty.quote',
          date: DateTime(1990),
        );
  Id? id;
  String? quote;
  String? author;
  DateTime? date;

  @override
  DailyQuoteEntity toEntity() =>
      DailyQuoteEntity(id: id, quote: quote, author: author, date: date);
}
