import 'package:dartz/dartz.dart';
import 'package:task_manager/core/errors/exception.dart';
import 'package:task_manager/core/errors/failure.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/mini_habit/data/datasources/mini_habit/mini_habit_data_source.dart';
import 'package:task_manager/src/mini_habit/domain/entities/category_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/daily_quote_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habit_entity.dart';
import 'package:task_manager/src/mini_habit/domain/entities/mini_habits_line_char_data.dart';
import 'package:task_manager/src/mini_habit/domain/repos/mini_habit_repo.dart';

class MiniHabitRepoImpl extends MiniHabitRepo {
  const MiniHabitRepoImpl(this._dataSource);
  final MiniHabitDataSource _dataSource;
  @override
  ResultFuture<List<MiniHabitEntity>> getAllActiveMiniHabits() async {
    final entities = <MiniHabitEntity>[];
    try {
      final collections = await _dataSource.getAllActiveMiniHabits();
      collections.map((col) {
        entities.add(
          MiniHabitEntity(
            id: col.id,
            title: col.title,
            description: col.description,
            category: col.category.value?.toEntity(),
          ),
        );
      }).toList();
      return Right(entities);
    } on ServerException catch (e) {
      return Left(ServerFailure.fromException(e));
    }
  }

  @override
  ResultFuture<DailyQuoteEntity> getQuoteOfTheDay() async {
    try {
      final result = await _dataSource.getQuoteOfTheDay();
      return result != null
          ? Right(result.toEntity())
          : Right(DailyQuoteEntity.defaultQuote());
    } on ServerException catch (e) {
      return Left(ServerFailure.fromException(e));
    }
  }

  @override
  ResultFuture<void> validateMiniHabit({
    required int id,
    required int resistance,
  }) async {
    try {
      await _dataSource.validateMiniHabit(id: id, resistance: resistance);
      return const Right(null);
    } on ServerException catch (e) {
      return Left(ServerFailure.fromException(e));
    }
  }

  @override
  ResultFuture<MiniHabitEntity> getMiniHabitById({required int id}) async {
    try {
      final result = await _dataSource.getMiniHabitById(id: id);
      return result != null
          ? Right(result.toEntity())
          : const Left(
              ServerFailure(message: 'Mini habit not found', statusCode: 404),
            );
    } on ServerException catch (e) {
      return Left(ServerFailure.fromException(e));
    }
  }

  @override
  ResultFuture<List<CategoryEntity>> getAllCategories() async {
    try {
      final result = await _dataSource.getAllCategories();
      return Right(result.map((e) => e.toEntity()).toList());
    } on ServerException catch (e) {
      return Left(ServerFailure.fromException(e));
    }
  }

  @override
  ResultFuture<CategoryEntity> createCategory({required String title}) async {
    try {
      final result = await _dataSource.createCategory(title: title);
      return Right(result.toEntity());
    } on ServerException catch (e) {
      return Left(ServerFailure.fromException(e));
    }
  }

  @override
  ResultFuture<void> updateMiniHabit({
    required MiniHabitEntity miniHabitEntity,
  }) async {
    try {
      await _dataSource.updateMiniHabit(
        miniHabitEntity: miniHabitEntity,
      );
      return const Right(null);
    } on ServerException catch (e) {
      return Left(ServerFailure.fromException(e));
    }
  }

  @override
  ResultFuture<MiniHabitsLineChartData> getMiniHabitLineCharData() async {
    try {
      final result = await _dataSource.getMiniHabitLineCharData();
      return Right(result.toEntity());
    } on ServerException catch (e) {
      return Left(ServerFailure.fromException(e));
    }
  }
}
