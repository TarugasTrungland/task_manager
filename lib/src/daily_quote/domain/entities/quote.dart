class Quote {
  const Quote({
    required this.id,
    required this.quote,
    required this.length,
    required this.author,
    required this.tags,
    required this.permalink,
    required this.title,
    required this.background,
    required this.date,
  });

  const Quote.empty()
      : this(
          id: '_empty.id',
          quote: '_empty.quote',
          length: 0,
          author: '_empty.author',
          tags: const ['_empty.tag'],
          permalink: '_empty.permalink',
          title: '_empty.title',
          background: '_empty.background',
          date: '_empty.date',
        );
  final String id;
  final String quote;
  final int length;
  final String author;
  final List<String> tags;
  final String permalink;
  final String title;
  final String background;
  final String date;

  Quote copyWith({
    String? id,
    String? quote,
    int? length,
    String? author,
    List<String>? tags,
    String? permalink,
    String? title,
    String? background,
    String? date,
  }) {
    return Quote(
      id: id ?? this.id,
      quote: quote ?? this.quote,
      length: length ?? this.length,
      author: author ?? this.author,
      tags: tags ?? this.tags,
      permalink: permalink ?? this.permalink,
      title: title ?? this.title,
      background: background ?? this.background,
      date: date ?? this.date,
    );
  }
}
