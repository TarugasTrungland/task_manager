import 'dart:ui';

import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/daily_quote/domain/entities/quote.dart';

abstract class QuoteRepo {
  const QuoteRepo();
  ResultFuture<Quote> getDailyQuote({required Locale lang});
}
