import 'dart:ui';

import 'package:task_manager/core/usecase/usecase.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/daily_quote/domain/entities/quote.dart';
import 'package:task_manager/src/daily_quote/domain/repos/quote_repo.dart';

class GetDailyQuote extends UsecaseWithParams<Quote, Locale> {
  GetDailyQuote(this._repo);
  final QuoteRepo _repo;

  @override
  ResultFuture<Quote> call(Locale params) => _repo.getDailyQuote(lang: params);
}
