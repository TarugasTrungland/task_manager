import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:task_manager/src/daily_quote/domain/entities/quote.dart';

class QuoteModel extends Quote with EquatableMixin {
  const QuoteModel({
    required super.id,
    required super.quote,
    required super.length,
    required super.author,
    required super.tags,
    required super.permalink,
    required super.title,
    required super.background,
    required super.date,
  });

  const QuoteModel.empty()
      : this(
          id: '_empty.id',
          quote: '_empty.quote',
          length: 0,
          author: '_empty.author',
          tags: const ['_empty.tag'],
          permalink: '_empty.permalink',
          title: '_empty.title',
          background: '_empty.background',
          date: '_empty.date',
        );

  factory QuoteModel.fromJson(String source) =>
      QuoteModel.fromMap(jsonDecode(source) as Map<String, dynamic>);

  QuoteModel.fromMap(Map<String, dynamic> map)
      : this(
          id: map['id'] as String,
          quote: map['quote'] as String,
          length: map['length'] as int,
          author: map['author'] as String,
          tags: List<String>.from(map['tags'] as List<dynamic>),
          permalink: map['permalink'] as String,
          title: map['title'] as String,
          background: map['background'] as String,
          date: map['date'] as String,
        );

  String toJson() => jsonEncode(toMap());

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'quote': quote,
      'length': length,
      'author': author,
      'tags': tags,
      'permalink': permalink,
      'title': title,
      'background': background,
      'date': date,
    };
  }

  @override
  QuoteModel copyWith({
    String? id,
    String? quote,
    int? length,
    String? author,
    List<String>? tags,
    String? permalink,
    String? title,
    String? background,
    String? date,
  }) {
    return QuoteModel(
      id: id ?? this.id,
      quote: quote ?? this.quote,
      length: length ?? this.length,
      author: author ?? this.author,
      tags: tags ?? this.tags,
      permalink: permalink ?? this.permalink,
      title: title ?? this.title,
      background: background ?? this.background,
      date: date ?? this.date,
    );
  }

  @override
  List<Object?> get props => [id];
}
