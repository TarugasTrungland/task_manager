import 'dart:ui';

import 'package:dartz/dartz.dart';
import 'package:task_manager/core/errors/exception.dart';
import 'package:task_manager/core/errors/failure.dart';
import 'package:task_manager/core/utils/typedef.dart';
import 'package:task_manager/src/daily_quote/data/datasources/quote_remote_datasource.dart';
import 'package:task_manager/src/daily_quote/domain/entities/quote.dart';
import 'package:task_manager/src/daily_quote/domain/repos/quote_repo.dart';

class QuoteRepoImpl extends QuoteRepo {
  const QuoteRepoImpl(this._remoteDatasource);
  final QuoteRemoteDatasource _remoteDatasource;

  @override
  ResultFuture<Quote> getDailyQuote({required Locale lang}) async {
    try {
      final result = await _remoteDatasource.getDailyQuote(lang: lang);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(message: e.message, statusCode: e.statusCode));
    }
  }
}
