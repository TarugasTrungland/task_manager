import 'dart:convert';
import 'dart:ui';
import 'package:http/http.dart' as http;
import 'package:task_manager/core/errors/exception.dart';

import 'package:task_manager/src/daily_quote/data/datasources/quote_api_constants.dart';
import 'package:task_manager/src/daily_quote/data/models/quote_model.dart';

abstract class QuoteRemoteDatasource {
  const QuoteRemoteDatasource();
  Future<QuoteModel> getDailyQuote({required Locale lang});
}

class QuoteRemoteDatasourceImpl extends QuoteRemoteDatasource {
  const QuoteRemoteDatasourceImpl(this._client);
  final http.Client _client;
  Map<String, dynamic> _decode(http.Response response) {
    return json.decode(utf8.decode(response.bodyBytes)) as Map<String, dynamic>;
  }

  @override
  Future<QuoteModel> getDailyQuote({required Locale lang}) async {
    try {
      final response = await _client.get(
        Uri.parse(
          QuoteApiConstants.kClientUrl + lang.languageCode.substring(0, 2),
        ),
        headers: QuoteApiConstants.kAdminHeaders(),
      );
      if (response.statusCode != 200 && response.statusCode != 201) {
        throw ServerException(
          message: response.body,
          statusCode: response.statusCode,
        );
      }
      final result = _decode(response);
      final quote = QuoteModel.fromMap(
        result['contents']['quotes'][0] as Map<String, dynamic>,
      );
      return quote;
    } on ServerException {
      rethrow;
    } catch (e) {
      throw ServerException(message: e.toString(), statusCode: 505);
    }
  }
}
