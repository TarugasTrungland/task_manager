class QuoteApiConstants {
  QuoteApiConstants._();
  static const String kUrl = 'https://quotes.rest/';
  static const String kClientApi = 'qod?category=inspire&language=';
  static const String kClientUrl = kUrl + kClientApi;

  static const String kClientApiKey =
      'xoI8ROOOJniwFy1N9eT9tZiYBJXRJG0iD3J6pamv';

  static Map<String, String> kAdminHeaders() {
    return {
      'accept': 'application/json',
      'X-TheySaidSo-Api-Secret': kClientApiKey,
    };
  }
}
