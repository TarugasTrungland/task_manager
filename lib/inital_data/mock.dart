//ignore_for_file: type_annotate_public_apis, prefer_single_quotes

class Mock {
  static const categoriesJson = {
    "categories": [
      {"title": "Category 01"},
      {"title": "Category 02"},
      {"title": "Category 03"},
    ],
  };
  static const miniHabitsJson = {
    "miniHabits": [
      {"title": "Mini Habit 01", "description": "Description 01"},
      {"title": "Mini Habit 02", "description": "Description 02"},
      {"title": "Mini Habit 03", "description": "Description 03"},
      {"title": "Mini Habit 04", "description": "Description 04"},
    ],
  };

  static const dailyQuote = {
    "quotes": [
      {
        "id": "Q3TIEXJ5M0DTgVJkdShQpAeF",
        "quote": "Lorem ipsum dolor sit amet",
        "length": 26,
        "author": "Abraham Lincoln",
        "language": "en",
        "tags": ["inspire", "future"],
        "sfw": "sfw",
        "permalink":
            " https://theysaidso.com/quote/abraham-lincoln-the-best-way-to-predict-the-future-is-to-create-it",
        "title": "Inspiring Quote of the day",
        "category": "inspire",
        "background":
            "https://theysaidso.com/assets/images/qod/qod-inspire.jpg",
        "date": "2024-04-22",
      }
    ],
  };
}
